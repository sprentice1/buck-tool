To use this application, you need to install LTSpice and Octave.
Octave must be set as an environment variable. To do so, go to your octave directory, navigate to mingw64,
and add the path to environment variable paths.
To install all dependencies, run the following:
pip install -r requirements.txt

All the packages should get install.

To run the app, run the python command_line_interface.py

You can manually enter all the parameters, or use the json files.
