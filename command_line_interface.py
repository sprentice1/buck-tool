from switcher_calculator_tool.Python_code.buck_calculator_tool import run_buck_tool
import tkinter as tk
from tkinter import filedialog
from Database.lib.database import Database


class Interface:
    def __init__(self):
        path_to_component_database = 'Database/lib/Component Database'
        # instance of database object
        self.db = Database(path_to_component_database)
        print("LTspice and octave must be installed. Octave must be set as environment variable ")
        self.start_menu()

    def start_menu(self):
        while True:
            print("This command line interface runs the buck converter simulator")
            print("Here are the options:")
            print("1 - Run simulation without LTspice from .json file")
            print("2 - Run simulation with LTspice from .json file")
            print("3 - Add/List components in the database")
            print("4 - Exit")
            option = input("Choose option: ")

            if option == "1":
                self.run_sim1()
            elif option == "2":
                self.run_sim2()
            elif option == "3":
                self.database()
            elif option == "4":
                return
            else:
                print("Input a valid option")

    def run_sim1(self):
        while True:
            # Opening file explorer to get path to json file
            root = tk.Tk()
            file_path = filedialog.askopenfilename()
            root.destroy()
            if file_path.endswith('.json'):
                run_buck_tool(False, file=file_path)
                return
            else:
                print("Enter file with .json extension")

    def run_sim2(self):
        # Opening file explorer to get path to json file
        root = tk.Tk()
        file_path = filedialog.askopenfilename()
        root.destroy()

        if file_path.endswith('.json'):
            run_buck_tool(True, file=file_path)
            return
        else:
            print("Enter file with .json extension")

    def database(self):
        # Database functions. Json file database is located in database directory under lib
        while True:
            print("Here are the database options:")
            print("1 - Add capacitor")
            print("2 - Add inductor")
            print("3 - list capacitors")
            print("4 - list inductors")
            print("5 - return to start menu")
            option = input("Choose option: ")

            if option == "1":
                self.add_cap()
            elif option == "2":
                self.add_ind()
            elif option == "3":
                self.db.list_cap_db()
            elif option == "4":
                self.db.list_ind_db()
            elif option == "5":
                return
            else:
                print("Input a valid option")

    def add_cap(self):
        while True:
            print("Enter the following values one at a time, fill all the fields. Set the ones not used to 0")
            name = input("Enter capacitor name: ")
            capacitance = float(input("Enter capacitance value: "))
            esr = float(input("Enter esr value: "))
            esl = float(input("Enter esl value: "))
            df = float(input("df: "))
            confirm = input("Confirm (y/N): ")

            if confirm == "y":
                cap_dict = {
                    'id': 0,
                    'name': name,
                    'capacitance': capacitance,
                    'esr': esr,
                    'esl': esl,
                    'df': df
                }
                self.db.add_capacitor(cap_dict)
                another = input("Want to add another (y/N): ")
                if another != "y":
                    return
            else:
                another = input("Want to try again (y/N): ")
                if another != "y":
                    return

    def add_ind(self):
        while True:
            print("Enter the following values one at a time, fill all the fields. Set the ones not used to 0")
            name = input("Enter inductor name: ")
            inductance = float(input("Enter inductance value: "))
            Cpl = int(input("Enter Cpl value default=1: "))
            dcr = float(input("Enter dcr value: "))
            lm = float(input("Lm default = 0: "))
            confirm = input("Confirm (y/N): ")

            if confirm == "y":
                ind_dict = {
                    'id': 0,
                    'name': name,
                    'Cpl': Cpl,
                    'Lk': inductance,
                    'dcr': dcr,
                    'lm': lm
                }
                self.db.add_inductor(ind_dict)
                another = input("Want to add another (y/N): ")
                if another != "y":
                    return
            else:
                another = input("Want to try again (y/N): ")
                if another != "y":
                    return


if __name__ == "__main__":
    # Command line interface object
    CLI = Interface()

