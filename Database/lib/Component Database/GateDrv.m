## Gate driver data
## 
## TODO : Correct parameter for gate drive voltage & temperature
## 
1;

## From ISL6308A datasheet
function [Tg, Bg] = GateDrv_ISL6308A(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.60, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.40, 2.20];
  Bg.Rpd = [0.60, 0.94, 1.35];
  Bg.Dt  = 10e-9;
endfunction

## From ISL6314 datasheet
function [Tg, Bg] = GateDrv_ISL6314(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.65, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.25, 2.20];
  Bg.Rpd = [0.60, 0.80, 1.35];
  Bg.Dt  = 10e-9;
endfunction

## From ISL6622 datasheet
function [Tg, Bg] = GateDrv_ISL6622(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.35, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.35, 2.20];
  Bg.Rpd = [0.60, 0.90, 1.35];
  Bg.Dt  = 10e-9;
endfunction

## From ??? datasheet, to be updated
function [Tg, Bg] = GateDrv_DRMos(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.65, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.25, 2.20];
  Bg.Rpd = [0.60, 0.80, 1.35];
  Bg.Dt  = 10e-9;
endfunction

## From ??? datasheet, to be updated
function [Tg, Bg] = GateDrv_AOZ1022D(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.65, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.25, 2.20];
  Bg.Rpd = [0.60, 0.80, 1.35];
  Bg.Dt  = 10e-9;
endfunction

## From ??? datasheet, to be updated
function [Tg, Bg] = GateDrv_SC417(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.65, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.25, 2.20];
  Bg.Rpd = [0.60, 0.80, 1.35];
  Bg.Dt  = 10e-9;
endfunction

## From ??? datasheet, to be updated
function [Tg, Bg] = GateDrv_IR3821(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.65, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.25, 2.20];
  Bg.Rpd = [0.60, 0.80, 1.35];
  Bg.Dt  = 10e-9;
endfunction
