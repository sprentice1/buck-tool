## This file constains MOSFET critical parameters
## 
## TODO : Correct parameters for gate drive voltage and temperature
## 
1;

## BSC014
function M = BSC014(Gv, T)
  M.Rds   = 1.4e-3*[0.75, 1, 1.25]; # At 5V, 25oC
  M.Rg    = [0.7, 1.5, 2.6];
  M.Vth   = [1, 1.5, 2];
  M.Vsp   = 2.6;
  M.Qt    = [37e-9, 55e-9, 73e-9];
  M.Qgs   = [18e-9, 26e-9, 34e-9];
  M.Qgd   = [4e-9, 13e-9, 22e-9];
  M.Qgth  = [11e-9, 16e-9, 21e-9];
  M.Qsw   = [12e-9, 23e-9, 35e-9];
  M.Coss  = 5e-9;
  M.Ciss  = 12e-9;
  M.D.Vf  = 0.8;
  M.D.C   = 300e-12;
endfunction

## BSC020
function M = BSC020(Gv, T)
  M.Rds   = 2.3e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  M.Rg    = [0.9, 1.9, 3.3];
  M.Vth   = [1, 1.6, 2.2];
  M.Vsp   = 2.8;
  M.Qt    = [24e-9, 34e-9, 45e-9];
  M.Qgs   = [11e-9, 15e-9, 20e-9];
  M.Qgd   = [4e-9, 7e-9, 12e-9];
  M.Qgth  = [7e-9, 9e-9, 12e-9];
  M.Qsw   = [9e-9, 14e-9, 21e-9];
  M.Coss  = 3.5e-9;
  M.Ciss  = 5.5e-9;
  M.D.Vf  = 0.8;
  M.D.C   = 300e-12;
endfunction

## BSC030
function M = BSC030(Gv, T)
  M.Rds   = 3.8e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  M.Rg    = [0.7, 1.5, 2.6];
  M.Vth   = [1, 1.6, 2.2];
  M.Vsp   = 3.0;
  M.Qt    = [13e-9, 20e-9, 27e-9];
  M.Qgs   = [6e-9, 9.5e-9, 13e-9];
  M.Qgd   = [1.6e-9, 4.6e-9, 7.6e-9];
  M.Qgth  = [4.5e-9, 5.2e-9, 6.9e-9];
  M.Qsw   = [5e-9, 9e-9, 13e-9];
  M.Coss  = 2.2e-9;
  M.Ciss  = 3.2e-9;
  M.D.Vf  = 0.8;
  M.D.C   = 300e-12;
endfunction

## BSC042
function M = BSC042(Gv, T)
  M.Rds   = 5.2e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  M.Rg    = [0.7, 1.5, 2.6];
xk
  M.Vth   = [1, 1.6, 2.2];
  M.Vsp   = 3.1;
  M.Qt    = [10e-9, 15e-9, 20e-9];
  M.Qgs   = [4.9e-9, 7.4e-9, 9.9e-9];
  M.Qgd   = [1.2e-9, 3.5e-9, 5.8e-9];
  M.Qgth  = [2.7e-9, 3.9e-9, 5.1e-9];
  M.Qsw   = [4.1e-9, 7e-9, 9.9e-9];
  M.Coss  = 1.8e-9;
  M.Ciss  = 2.5e-9;
  M.D.Vf  = 0.8;
  M.D.C   = 300e-12;
endfunction

## BSC057
function M = BSC057(Gv, T)
  M.Rds   = 6.8e-3*[0.75, 1, 1.25]; # At 5V, 25oC
  M.Rg    = [0.6, 1.3, 2.3];
  M.Vth   = [1, 1.6, 2.2];
  M.Vsp   = 3.2;
  M.Qt    = [8e-9, 11e-9, 14e-9];
  M.Qgs   = [3.6e-9, 5.5e-9, 7.4e-9];
  M.Qgd   = [0.8e-9, 2.5e-9, 4.2e-9];
  M.Qgth  = [1.9e-9, 2.8e-9, 3.7e-9];
  M.Qsw   = [2.7e-9, 5.3e-9, 7.9e-9];
  M.Coss  = 1.4e-9;
  M.Ciss  = 2.0e-9;
  M.D.Vf  = 0.8;
  M.D.C   = 300e-12;
endfunction

## BSC080
function M = BSC080(Gv, T)
  M.Rds   = 9.6e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  M.Rg    = [0.5, 1.0, 1.8];
  M.Vth   = [1, 1.6, 2.2];
  M.Vsp   = 3.4;
  M.Qt    = [5e-9, 7.5e-9, 10e-9];
  M.Qgs   = [3.7e-9, 4e-9, 5.3e-9];
  M.Qgd   = [0.6e-9, 1.8e-9, 3.0e-9];
  M.Qgth  = [0.7e-9, 1.9e-9, 2.6e-9];
  M.Qsw   = [2.0e-9, 3.9e-9, 5.8e-9];
  M.Coss  = 1.0e-9;
  M.Ciss  = 1.5e-9;
  M.D.Vf  = 0.8;
  M.D.C   = 300e-12;
endfunction

## Internal AOZ1022D, need to find propoer parameters
function [Mt, Mb] = M_AOZ1022D(Gv, T)
  Mt.Rds   = 9.6e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  Mt.Rg    = [0.5, 1.0, 1.8];
  Mt.Vth   = [1, 1.6, 2.2];
  Mt.Vsp   = 3.4;
  Mt.Qt    = [5e-9, 7.5e-9, 10e-9];
  Mt.Qgs   = [3.7e-9, 4e-9, 5.3e-9];
  Mt.Qgd   = [0.6e-9, 1.8e-9, 3.0e-9];
  Mt.Qgth  = [0.7e-9, 1.9e-9, 2.6e-9];
  Mt.Qsw   = [2.0e-9, 3.9e-9, 5.8e-9];
  Mt.Coss  = 1.0e-9;
  Mt.Ciss  = 1.5e-9;
  Mt.D.Vf  = 0.8;
  Mt.D.C   = 300e-12;

  Mb.Rds   = 9.6e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  Mb.Rg    = [0.5, 1.0, 1.8];
  Mb.Vth   = [1, 1.6, 2.2];
  Mb.Vsp   = 3.4;
  Mb.Qt    = [5e-9, 7.5e-9, 10e-9];
  Mb.Qgs   = [3.7e-9, 4e-9, 5.3e-9];
  Mb.Qgd   = [0.6e-9, 1.8e-9, 3.0e-9];
  Mb.Qgth  = [0.7e-9, 1.9e-9, 2.6e-9];
  Mb.Qsw   = [2.0e-9, 3.9e-9, 5.8e-9];
  Mb.Coss  = 1.0e-9;
  Mb.Ciss  = 1.5e-9;
  Mb.D.Vf  = 0.8;
  Mb.D.C   = 300e-12;
endfunction

## Internal SC417, need to find propoer parameters
function [Mt, Mb] = M_SC417(Gv, T)
  Mt.Rds   = 9.6e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  Mt.Rg    = [0.5, 1.0, 1.8];
  Mt.Vth   = [1, 1.6, 2.2];
  Mt.Vsp   = 3.4;
  Mt.Qt    = [5e-9, 7.5e-9, 10e-9];
  Mt.Qgs   = [3.7e-9, 4e-9, 5.3e-9];
  Mt.Qgd   = [0.6e-9, 1.8e-9, 3.0e-9];
  Mt.Qgth  = [0.7e-9, 1.9e-9, 2.6e-9];
  Mt.Qsw   = [2.0e-9, 3.9e-9, 5.8e-9];
  Mt.Coss  = 1.0e-9;
  Mt.Ciss  = 1.5e-9;
  Mt.D.Vf  = 0.8;
  Mt.D.C   = 300e-12;

  Mb.Rds   = 9.6e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  Mb.Rg    = [0.5, 1.0, 1.8];
  Mb.Vth   = [1, 1.6, 2.2];
  Mb.Vsp   = 3.4;
  Mb.Qt    = [5e-9, 7.5e-9, 10e-9];
  Mb.Qgs   = [3.7e-9, 4e-9, 5.3e-9];
  Mb.Qgd   = [0.6e-9, 1.8e-9, 3.0e-9];
  Mb.Qgth  = [0.7e-9, 1.9e-9, 2.6e-9];
  Mb.Qsw   = [2.0e-9, 3.9e-9, 5.8e-9];
  Mb.Coss  = 1.0e-9;
  Mb.Ciss  = 1.5e-9;
  Mb.D.Vf  = 0.8;
  Mb.D.C   = 300e-12;
endfunction

## Internal IR3821, need to find propoer parameters
function [Mt, Mb] = M_IR3821(Gv, T)
  Mt.Rds   = 9.6e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  Mt.Rg    = [0.5, 1.0, 1.8];
  Mt.Vth   = [1, 1.6, 2.2];
  Mt.Vsp   = 3.4;
  Mt.Qt    = [5e-9, 7.5e-9, 10e-9];
  Mt.Qgs   = [3.7e-9, 4e-9, 5.3e-9];
  Mt.Qgd   = [0.6e-9, 1.8e-9, 3.0e-9];
  Mt.Qgth  = [0.7e-9, 1.9e-9, 2.6e-9];
  Mt.Qsw   = [2.0e-9, 3.9e-9, 5.8e-9];
  Mt.Coss  = 1.0e-9;
  Mt.Ciss  = 1.5e-9;
  Mt.D.Vf  = 0.8;
  Mt.D.C   = 300e-12;

  Mb.Rds   = 9.6e-3*[0.8, 1, 1.2]; # At 5V, 25oC
  Mb.Rg    = [0.5, 1.0, 1.8];
  Mb.Vth   = [1, 1.6, 2.2];
  Mb.Vsp   = 3.4;
  Mb.Qt    = [5e-9, 7.5e-9, 10e-9];
  Mb.Qgs   = [3.7e-9, 4e-9, 5.3e-9];
  Mb.Qgd   = [0.6e-9, 1.8e-9, 3.0e-9];
  Mb.Qgth  = [0.7e-9, 1.9e-9, 2.6e-9];
  Mb.Qsw   = [2.0e-9, 3.9e-9, 5.8e-9];
  Mb.Coss  = 1.0e-9;
  Mb.Ciss  = 1.5e-9;
  Mb.D.Vf  = 0.8;
  Mb.D.C   = 300e-12;
endfunction
