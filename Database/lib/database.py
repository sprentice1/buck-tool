from tinydb import TinyDB, Query


class Database:
    def __init__(self, path):
        self.cap_database = TinyDB(path+'/Capacitor.json')
        self.ind_database = TinyDB(path+'/Inductor.json')
        self.User = Query()

    def add_capacitor(self, cap_dict):
        if self.cap_database.search(self.User.name == cap_dict['name']):
            print('Value already in db')
        else:
            cap_dict['id'] = len(self.cap_database)+1
            self.cap_database.insert(cap_dict)

    def get_capacitor(self, name):
        cap = self.cap_database.search(self.User.name == name)[0]
        if cap != None:
            return cap
        else:
            print('Not in database')

    def add_inductor(self, ind_dict):
        if self.ind_database.search(self.User.name == ind_dict['name']):
            print('Value already in db')
        else:
            ind_dict['id'] = len(self.ind_database) + 1
            self.ind_database.insert(ind_dict)

    def get_inductor(self, name):
        ind = self.ind_database.search(self.User.name == name)[0]
        if ind != None:
            return ind
        else:
            print('Not in database')

    def get_capacitor_value(self, value):
        cap = self.cap_database.search(self.User.capacitance == value)[0]
        if cap != None:
            return cap
        else:
            print('Not in database')

    def get_capacitor_id(self, id):
        if type(id) == int:
            cap = self.cap_database.search(self.User.id == id)[0]
        else:
            cap = self.cap_database.search(self.User.name == id)[0]
        if cap != None:
            return cap
        else:
            print('Not a valid id')

    def get_inductor_id(self, id):
        if type(id) == int:
            ind = self.ind_database.search(self.User.id == id)[0]
        else:
            ind = self.ind_database.search(self.User.name == id)[0]
        if ind != None:
            return ind
        else:
            print('Not a valid id')

    def list_cap_db(self):
        cap = self.cap_database.search(self.User)

        for i in range(0, len(cap)):
            print("id: {:d}, name: {:s}, capacitance: {:.2f}uF, esr: {:.2f}mOhm, esl: {:.2f}nH, df: {:.2f}".format(cap[i]['id'], cap[i]['name'], 1e6*cap[i]['capacitance'], 1e3*cap[i]['esr'], 1e9*cap[i]['esl'], cap[i]['df']))

    def list_ind_db(self):
        ind = self.ind_database.search(self.User)

        for i in range(0, len(ind)):
            print("id: {:d}, name: {:s}, inductance: {:.2f}uH, Cpl: {:.2f}, dcr: {:.2f}mOhm, lm: {:.2f}".format(ind[i]['id'], ind[i]['name'], 1e6*ind[i]['Lk'], ind[i]['Cpl'], 1e3*ind[i]['dcr'], ind[i]['lm']))


if __name__ == "__main__":
    cap_dict = {
        'id': 0,
        'name': 'C_10u_25V_X5R_1206',
        'capacitance': 10e-6,
        'esr': 4.6e-3,
        'esl': 1.2e-9,
        'df': 0
    }

    ind_dict = {
        'id': 0,
        'name': 'L_680n_10A_9mR_2020',
        'Cpl': 1,
        'Lk': 680e-9,
        'dcr': 9e-3,
        'lm': 0
    }

    db = Database('Component Database')

    #db.add_capacitor(cap_dict)
    #db.add_inductor(ind_dict)

    db.list_cap_db()
    print()
    db.list_ind_db()
    print(db.get_inductor_id(2))
