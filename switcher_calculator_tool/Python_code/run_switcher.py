from oct2py import octave


# To add a switcher to the app, add it to the get_switcher method, and add a run_switcher method
class Switcher:
    def get_switcher(self, P, C, PS, name):
        if name == 'SC431A':
            P, C = self.run_SC431A(P, C, PS)
            return P, C
        elif name == 'SC402B':
            P, C = self.run_SC402B(P, C, PS)
            return P, C
        elif name == 'NB681A':
            P, C = self.run_NB681A(P, C, PS)
            return P, C
        elif name == 'SC417':
            P, C = self.run_SC417(P, C, PS)
            return P, C
        elif name == 'SC401B':
            P, C = self.run_SC401B(P, C, PS)
            return P, C
        else:
            print('The switcher chosen does not exist')
            return -1

    def run_SC431A(self, P, C, PS):
        P, C = octave.SC431A(P, C, PS, 'test', nout=2)
        return P, C

    def run_SC401B(self, P, C, PS):
        PS.Tf, PS.Bf = octave.M_SC417(P.Gv, P.t, 'SC417', nout=2)
        PS.D = PS.Bf.D
        PS.Tfdr, PS.Bfdr = octave.GateDrv_SC417(P.Gv, P.t, nout=2)
        P, C = octave.SC401B(P, C, PS, 'test', nout=2)
        R = octave.Pstage(P, PS)
        return P, C

    def run_SC402B(self, P, C, PS):
        PS.Tf, PS.Bf = octave.M_SC417(P.Gv, P.t, 'SC417', nout=2)
        PS.D = PS.Bf.D
        PS.Tfdr, PS.Bfdr = octave.GateDrv_SC417(P.Gv, P.t, nout=2)
        P, C = octave.SC402B(P, C, PS, 'test', nout=2)
        R = octave.Pstage(P, PS)
        return P, C

    def run_SC417(self, P, C, PS):
        PS.Tf, PS.Bf = octave.M_SC417(P.Gv, P.t, 'SC417', nout=2)
        PS.D = PS.Bf.D
        PS.Tfdr, PS.Bfdr = octave.GateDrv_SC417(P.Gv, P.t, nout=2)
        P, C = octave.SC417(P, C, PS, 'test', nout=2)
        R = octave.Pstage(P, PS)
        return P, C

    def run_NB681A(self, P, C, PS):
        P, C = octave.NB681A(P, C, PS, 'test', nout=2)
        return P, C

