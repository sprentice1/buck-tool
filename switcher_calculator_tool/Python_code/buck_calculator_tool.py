import numpy as np
from oct2py import octave
import matplotlib.pyplot as plt
from switcher_calculator_tool.Python_code.plot_ltspice import plot_output
import json
from switcher_calculator_tool.Python_code import run_switcher
from Database.lib.database import Database

octave.addpath("Octave_files/Switchers")
octave.addpath("Octave_files/Mosfets")
octave.addpath("Octave_files/GateDrv")
octave.addpath("Octave_files")


def run_buck_tool(ltspice_sim, sim_params=None, file=None):
    # The simulation takes a couple minutes
    run_ltspice_sim = ltspice_sim

    path_to_component_database = 'Database/lib/Component Database'

    db = Database(path_to_component_database)
    if sim_params == None and file != None:
        with open(file) as json_file:
            sim_params = json.load(json_file)

    # Switcher parameters
    vout = float(sim_params['vout'])
    vin = float(sim_params['vin']) # need to put decimals even if whole number, python automatically casts round numbers as int
    iout = float(sim_params['iout'])
    fsw = float(sim_params['fsw'])
    max_voltage = float(sim_params['max_voltage'])
    vdd = float(sim_params['vdd'])
    ilim = float(sim_params['ilim'])
    Rilim = float(sim_params['Rilim']) # 446.00*ilim*(0.099*(5.00-vdd) + 1)
    Css = float(sim_params['Css']) # soft start capacitor

    # Switcher output resistors
    R1 = float(sim_params['R1'])
    R2 = float(sim_params['R2'])
    R3 = float(sim_params['R3'])
    Rton = float(sim_params['Rton'])
    C1 = float(sim_params['C1'])

    # Capacitor and inductor settings
    # First output capacitor
    cap_params1_output = db.get_capacitor_id(sim_params['cap_out1'])
    cap_params1_output['n_parallel1'] = sim_params['n_parallel_out1']

    # Second output capacitor
    if sim_params['cap_out2'] != 0:
        cap_params2_output = db.get_capacitor_id(sim_params['cap_out2'])
        cap_params2_output['n_parallel2'] = sim_params['n_parallel_out2']
    else:
        cap_params2_output = {}

    # Input capacitor
    cap_params_input = db.get_capacitor_id(sim_params['cap_input'])
    cap_params_input['n_parallel1'] = sim_params['n_parallel_in']

    # Output inductor
    inductor_params_output = db.get_inductor_id(sim_params['ind_out'])

    # Input ferrite bead or inductor
    inductor_params_input = db.get_inductor_id(sim_params['ind_in'])

    sw_name = sim_params['sw_name']


    # Capacitor and resistor tolerances
    Ctol = np.array([0.85, 1, 1.15])
    # Rtol is the tolerance for R1 and R2
    Rtol = np.array([0.999, 1, 1.001])
    # Rtol2 is the tolerance for R3
    Rtol2 = np.array([0.99, 1, 1.01])

    # The following classes  are to convert the json data into objects to send to octave functions
    class P:
        def __init__(self):
            self.vin = np.array([vin*0.95, vin*1, vin*1.05])
            self.vout = vout
            self.vdd = sim_params['vdd']
            self.iout = [iout, iout, iout]
            self.ioc = ilim
            self.load_line = [0, 0, 0]
            self.phase = 1
            self.fsw = fsw
            self.t = [0, 25, 70] # Operating temperature
            self.Gv = vin  # Gate voltage
            if sim_params['vdd'] < 3.6:
                self.k = (sim_params['vdd']-1.75)*10.0/vin
            else:
                self.k = 1.0
            self.vpeak = max_voltage

    # Config parameters for switchers
    class C:
        def __init__(self):
            if R1 != 0:
                self.R1 = R1 * Rtol
            if R2 != 0:
                self.R2 = R2 * Rtol
            if R3 != 0:
                self.R3 = R3 * Rtol2
            if Css != 0:
                self.Css = Css * Ctol
            if Rilim != 0:
                self.Rilim = Rilim * Rtol
            if C1 != 0:
                self.C1 = C1 * Ctol
            if Rton != 0:
                self.Rton = Rton * Rtol2

    # Capacitor class
    class Cap:
        def __init__(self, cap1, cap2={}):
            if cap2 == {}:
                self.C = (cap1['n_parallel1'] * cap1['capacitance']) * Ctol
                self.esr = (1.00 / (cap1['n_parallel1'] * (1.00 / cap1['esr']))) * Ctol
                self.esl = cap1['esl'] * Ctol
                print("--------------------test_esr1------------------------")
                print(self.esr)
            elif (cap1['esl'] == 0) or (cap2['esl'] == 0):
                self.C = (cap1['n_parallel1'] * cap1['capacitance'] + cap2['n_parallel2'] * cap2['capacitance']) * Ctol
                self.esr = (1.00 / ((1.00 / (cap1['esr']/cap1['n_parallel1'])) + (1.00 / (cap2['esr']/cap2['n_parallel2']))))*Ctol
                print("--------------------test_esr2------------------------")
                print(self.esr)
                self.esl = 0.00
            else:
                self.C = (cap1['n_parallel1'] * cap1['capacitance'] + cap2['n_parallel2'] * cap2['capacitance']) * Ctol
                self.esr = (1.00 / ((1.00*cap1['n_parallel1'] / (cap1['esr'])) + (1.00*cap2['n_parallel2'] / cap2['esr'])))*Ctol
                self.esl = 1.00 / (1.00 / cap1['esl'] + 1.00 / cap2['esl']) * Ctol
                print("--------------------test_esr3------------------------")
                print(self.esr)
                print(cap1['n_parallel1'])
                print(cap2['n_parallel2'])

    # Inductor class
    class L:
        def __init__(self, ind_params):
            self.dcr = ind_params['dcr'] * Ctol
            self.Lk = ind_params['Lk'] * Ctol
            self.L = self.Lk
            self.Cpl = 1
            self.Lm = 0

    # Cap and inductor parameter class
    class PS:
        def __init__(self):
            self.Lo = L(inductor_params_output)
            self.Co = Cap(cap_params1_output, cap2=cap_params2_output)
            self.Li = L(inductor_params_input)
            self.Ci = Cap(cap_params_input)

    P = P()
    C = C()
    PS = PS()

    switcher = run_switcher.Switcher()
    P, C = switcher.get_switcher(P, C, PS, sw_name)

    if run_ltspice_sim:
        # Plot output Vout
        print("This may take a while")
        steady_state = True
        x_val1, y_val1, input_val1, iout1, time1 = plot_output(int(P.fsw[0, 1]), float(P.vin[0, 1]), round(P.vout[0, 1], 3), float(P.iout[0, 1]), PS,
                    steady_state=steady_state)
        x_val0, y_val0, input_val0, iout0, time0 = plot_output(int(P.fsw[0, 0]), float(P.vin[0, 0]), round(P.vout[0, 0], 3), float(P.iout[0, 0]), PS,
                    steady_state=steady_state)
        x_val2, y_val2, input_val2, iout2, time2 = plot_output(int(P.fsw[0, 2]), float(P.vin[0, 2]), round(P.vout[0, 2], 3), float(P.iout[0, 2]), PS,
                    steady_state=steady_state)


        plt.figure()
        #plt.plot(time0, input_val0, label="min input")
        plt.plot(time1, input_val1, label="typ input")
        #plt.plot(time2, input_val2, label="max input")
        plt.title("Switcher {:.2f}V input".format(vin))
        plt.xlabel("time (s)")
        plt.ylabel("voltage (V)")
        if steady_state:
            plt.xlim((0.80 * time1[-1], 0.85 * time1[-1]))
        plt.legend()

        plt.figure()
        plt.plot(x_val0, y_val0,
                 label="{:s} (LTSpice output, C={:.2f}uF, L={:.2f}uH)".format('min', PS.Co.C[1] * 1000000,
                                                                              PS.Lo.Lk[1] * 1000000))
        plt.plot(x_val1, y_val1,
                 label="{:s} (LTSpice output, C={:.2f}uF, L={:.2f}uH)".format('typ', PS.Co.C[1] * 1000000,
                                                                              PS.Lo.Lk[1] * 1000000))
        plt.plot(x_val2, y_val2,
                 label="{:s} (LTSpice output, C={:.2f}uF, L={:.2f}uH)".format('max', PS.Co.C[1] * 1000000,
                                                                              PS.Lo.Lk[1] * 1000000))
        plt.title("Switcher {:.2f}V out".format(vout))
        plt.xlabel("time (s)")
        plt.ylabel("voltage (V)")
        if steady_state:
            plt.ylim((0.8 * vout, 1.2 * vout))
            plt.xlim((0.8 * time1[-1], 0.85*time1[-1]))
        plt.legend()


        plt.figure()
        #plt.plot(x_val0, iout0,
        #         label="{:s} (LTSpice output, C={:.2f}uF, L={:.2f}uH)".format('min', PS.Co.C[1] * 1000000,
        #                                                                      PS.Lo.Lk[1] * 1000000))
        plt.plot(x_val1, iout1,
                 label="{:s} (LTSpice output, C={:.2f}uF, L={:.2f}uH)".format('typ', PS.Co.C[1] * 1000000,
                                                                              PS.Lo.Lk[1] * 1000000))
        #plt.plot(x_val2, iout2,
        #         label="{:s} (LTSpice output, C={:.2f}uF, L={:.2f}uH)".format('max', PS.Co.C[1] * 1000000,
        #                                                                      PS.Lo.Lk[1] * 1000000))
        plt.title("Switcher {:.2f}A iout".format(iout))
        plt.xlabel("time (s)")
        plt.ylabel("current (A)")
        if steady_state:
            plt.ylim((0.5 * iout, 2 * iout))
            plt.xlim((0.8 * time1[-1], 0.85 * time1[-1]))
        plt.legend()
        plt.show()


if __name__ == "__main__":
    run_buck_tool(False)
