import numpy as np
from switcher_calculator_tool.LTspice.apply_ltspice_filter import apply_ltspice_filter
from scipy import signal as sp

##################################################
##             generate test signal             ##
##################################################


def plot_output(fsw, vin, vout, iout, PS, steady_state=True):
    # our samples shall be 100 ms wide
    vout_comp = vout + (iout*0.95) * PS.Lo.dcr[1] # Current offset, need to find error, temp patch on iout
    sample_width = 1000.0*(1.0/fsw)
    # time step between samples
    delta_t = 1.0/(fsw*300.0)
    samples = int(sample_width / delta_t)

    time = np.linspace(0, sample_width, samples)
    d = vout_comp/vin

    signal_a = vin/2+(vin/2)*sp.square(2*np.pi*fsw*time, duty=d)
    if steady_state:
        signal_a[0:1] = vout_comp
    else:
        signal_a[0:1] = 0

    ##################################################
    ##        apply filter - configuration 1        ##
    ##################################################

    # all values in SI units
    configuration = {
        "C": PS.Co.C[1],
        "L": PS.Lo.Lk[1],
        "ESR": PS.Co.esr[1],
        "DCR": PS.Lo.dcr[1],
        "R": vout/iout,
        # PWM params
        "vin": vin,
        "tr": 1e-9,
        "tf": 1e-9,
        "to": d*(1.0/fsw),
        "tp": (1.0/fsw)
    }

    signal_b1_x, signal_b1_y, signal_b1_iout = apply_ltspice_filter(
        "Buck Converter.asc",
        time, signal_a,
        params=configuration
    )

    return signal_b1_x, signal_b1_y, signal_a, signal_b1_iout, time
