# Design equation for VDDQ based on SC402B

clear all

source("../lib/Mosfets.m");
source("../lib/GateDrv.m");
source("../lib/Capacitors.m");
source("../lib/Inductors.m");
addpath("../lib");

Ctol = [0.85, 1, 1.15];
Rtol = [0.99, 1, 1.01];
Rtol2= [0.999, 1, 1.001];

# Converter parameters and requirements
P.vin       = 3.3*[0.95, 1, 1.05];
P.vout      = 1.14;
P.iout      = [0.5, 3, 5];
P.ioc       = 7;
P.load_line = [0,0,0];
P.phase     = 1;
P.fsw       = 500e3;
P.t         = [0, 25, 70];          # Operating temperature
P.Gv        = 12;                   # Gate voltage
P.k         = 1;

# Controller selected parameter
#C.R1        = 1.5e3*Rtol2;
C.Css       = 10e-9*Ctol;

# Computed and then selected parameters
C.R2        = 1.5e3*Rtol2;
#C.R3        = 75e3*Rtol;
C.C1        = 2.2e-9*Ctol;
C.Rton      = 75e3*Rtol;
C.Rilim     = 4.75e3*Rtol;

#
# power stage components
#
# Input filter
PS.Li       = Inductors_get(L_80n_6A_4mR_1206(0, P.t), 1);
PS.Ci       = Capacitors_get(C_10u_25V_X5R_1206(P.vin(2), P.t), 4);
# Output filter
PS.Lo       = Inductors_get(L_1u0_11A_9mR_2525(0, P.t), 1);
PS.Co       = Capacitors_get(C_470u_2V_POSCAP_7343(0, 0), 0,
                             C_100u_6V3_X5R_1210(0, 0), 8,
                             C_10u_6V3_X5R_0603(0, 0), 16);
# MOSFET
[PS.Tf, PS.Bf]     = M_SC417(P.Gv, P.t);
PS.D               = PS.Bf.D;
[PS.Tfdr, PS.Bfdr] = GateDrv_SC417(P.Gv, P.t);

# Run control component selection
[P, C] = SC402B(P, C, PS, "vddq");
# Run power stage computation
R = Pstage(P, PS);
