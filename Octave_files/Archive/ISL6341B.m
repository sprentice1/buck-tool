# Compute components from targets or compute
# achieved parameters versus targets using supplied components
# values.
#
# P   : Target parameters
# C   : Control components selection
# PS  : Power stage components selection
# name: name for plot
#
# TODO:
#       Modify compensation to design for worst case and plot
#       typ, min, max (monte carlo ?)
#       Consider DAC accuracy variation for Vout computation
#
function [P, C] = ISL6341B(P, C, PS, name)
  # Chip parametersmix of TI/Intersil/ST
  Prm.vref     = [0.7936, 0.8, 0.8064];
  Prm.en_pwr_r = [3.80, 4.20, 4.60];
  Prm.en_pwr_h = [0.15, 0.48, 0.85];
  Prm.ioc      = [9, 10, 11]*1e-6;
  Prm.fsw      = [540, 600, 660]*1e3;
  Prm.tss      = [0.9, 1, 1.1]*4e-3;
  Prm.Dmin     = 0;
  Prm.Dmax     = 0.75;
  
  Ctol = [0.95, 1, 1.05];
  Rtol = [0.99, 1, 1.01];

  ## 
  ## Components selection
  ## 
  printf("=== ISL6341B component selection ===\n\n");

  if P.phase != 1
    error("Number of phase must be one")
  endif

  printf("Selected components:\n\n")

  ## Output voltage
  if isfield(C, "R1") & !isfield(C, "R4")
    C.R4 = Prm.vref(2)*C.R1(2)/(P.vout-Prm.vref(2))*Rtol;
  elseif !isfield(C, "R1") & isfield(C, "R4")
    C.R1 = (P.vout - Prm.vref(2))*C.R4(2)./Prm.vref(2)*Rtol;
  elseif !isfield(C, "R1") & !isfield(C, "R4")
    error("R1 or R4 must be provided")
  endif
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (R1)", C.R1(2)/1e3, "kOhms");
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (R4)", C.R4(2)/1e3, "kOhms");

  ## Overcurrent
  if !isfield(C, "Rocset")
    C.Rocset    = P.ioc*PS.Bf.Rds(2)/Prm.ioc(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Over current setting resistor (Rocset)", C.Rocset(2)/1e3, "kOhms");

  printf("\n");

  ##
  ## Compute and printout achieved parameters
  ##
  printf("Achieved parameters:\n")
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");

  ## switching frequency
  P.fsw = Prm.fsw;
  fsw = Prm.fsw/1000;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");

  ## output voltage
  P.vout = Prm.vref.*(1+C.R1./fliplr(C.R4));
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
         " Output voltage", P.vout(1), P.vout(2), P.vout(3), "V");

  ## Duty cycle limitation (approximation, no losses)
  P.duty = P.vout./fliplr(P.vin);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
         " Duty cycle", P.duty(1)*100, P.duty(2)*100, P.duty(3)*100, "%");
  if P.duty < Prm.Dmin || P.duty > Prm.Dmax
    printf(" --> Duty cycle limitation")
  endif

  ## overcurrent
  P.ioc_pk = Prm.ioc.*C.Rocset./fliplr(PS.Bf.Rds);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
         " Overcurrent (peak, no heating)", P.ioc_pk(1), P.ioc_pk(2), \
         P.ioc_pk(3), "A");

  ## overcurrent (mean, approximation, no losses)
  dIL = P.vout.*(1-P.duty)./fliplr(PS.Lo.Lk.*Prm.fsw);
  P.ioc_avg = P.ioc_pk - fliplr(dIL/2);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
         " Overcurrent (average, no heating)", P.ioc_avg(1), \
         P.ioc_avg(2), P.ioc_avg(3), "A");

  ## soft start
  Tss   = Prm.tss*1e3;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
         " Soft start", Tss(1), Tss(2), Tss(3), "ms");

  ## Power good
  #P.Vpwrdgh = P.vout.*(Prm.vpgl+Prm.vpgh);
  #P.Vpwrdgl = P.vout.*Prm.vpgl;
  #printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
  #       " Power good rising threshold", P.Vpwrdgh(1), P.Vpwrdgh(2), P.Vpwrdgh(3), "V");
  #printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
  #       " Power good falling threshold", P.Vpwrdgl(1), P.Vpwrdgl(2), P.Vpwrdgl(3), "V");

  printf("\n");


  ## 
  ## Compensation design
  ## 
  printf("=== ISL6341B compensation design ===\n\n");
  if !isfield(C, "R1")
    warning("R1 not specified, compensation skipped\n")
  else
    if !isfield(C, "R2") | !isfield(C, "R3") | !isfield(C, "C1") | \
          !isfield(C, "C2") | !isfield(C, "C3")
      dotest = 0;
    else
      dotest = 1;
    endif

    design.R1 = C.R1(2);

    design.Vin  = P.vin(3);     # Design stability for maximum voltage voltage
    design.Lout = PS.Lo.Lk(2);
    design.DCR  = PS.Lo.dcr(2);
    Ceq         = 1./sum(1./(PS.Co.esr + 1./(2*pi*P.DBW*PS.Co.C*i)), 1); # effective at DBW
    design.Cout = -1./(2*pi*P.DBW*imag(Ceq(2)));
    design.ESR  = real(Ceq(2));
    design.Fsw  = P.fsw(2);
    design.G    = 1.0;          # Remote sense amplifier gain. If ever a
                                # voltage dividor is used, this must be changed.
    design.Fmax = 1e6;
    design.Fmin = 10;
    
    control.Vosc = 1.5;         # From ISL6341B datasheet,
    control.GBW  = 15e6;        # error amp gain bandwidth (TI)
    control.DCG  = 96;		# error amp DC gain in dB
    if !isfield(P, "DBW")
      design.DBW = P.fsw(1)/5;
    else
      design.DBW = P.DBW;
      if design.DBW > P.fsw(1)/3
        error("Converter bandwidth must be lower than Fsw/3")
      endif
    endif
    design.K     = P.K;

    if P.Type == 3
      if dotest == 0
        [sys, comp] = do_comp(design, control, "type3");
      else
        comp.R1 = design.R1;
        comp.R2 = C.R2(2);
        comp.R3 = C.R3(2);
        comp.C1 = C.C1(2);
        comp.C2 = C.C2(2);
        comp.C3 = C.C3(2);
        [sys, comp] = do_comp(design, control, "type3", comp);
      endif
    else
      error("To do")
    endif
    print_comp("type3", comp);
    plot_comp(sys, 1, name);

    printf("\n");
  endif  
endfunction
