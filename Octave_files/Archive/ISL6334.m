# Compute components from targets or compute
# achieved parameters versus targets using supplied components
# values.
#
# P   : Target parameters
# C   : Control components selection
# PS  : Power stage components selection
# TODO:
#       -Implement compensation design with worst case and plot
#        typ, min, max (monte carlo ?)
#       -Consider DAC accuracy variation for Vout computation
#       -Over current still not exact, need to correct effect of
#        multiphase ripple
#
function [P, C] = ISL6334(P, C, PS)
  # Chip parameters
  Prm.ref_tol  = [0.995, 1, 1.005];
  Prm.en_pwr_r = [0.875, 0.897, 0.920];
  Prm.en_pwr_f = [0.735, 0.752, 0.770];
  Prm.vofsn    = [0.39, 0.40, 0.415];
  Prm.vofsp    = [1.574, 1.60, 1.635];
  Prm.iocppk   = [115, 129, 146]*1e-6;
  Prm.iocavg   = [96, 105, 117]*1e-6;
  Prm.imon_oc  = [1.085, 1.11, 1.14];
  Prm.freq_tol = [0.9, 1.0, 1.1];
  Prm.Vboot    = 1.1;

  Ctol = [0.95, 1, 1.05];
  Rtol = [0.99, 1, 1.01];

  ## 
  ## Components selection
  ## 
  printf("=== ISL6334 component selection ===\n\n");

  if P.phase > 4 || P.phase < 1
    error("Number of phase must be from 1 to 4")
  endif

  printf("Selected components:\n")

  ## DAC setting is per user
  if !isfield(P, "dac")
    error("DAC setting needs to be provided")
  endif
  printf("%-45s : %6.2f %6s\n", " Selected DAC setting", P.dac, "Volt");

  ## Soft start
  if !isfield(C, "Rss")
    error("Rss must be specified")
  endif
  printf("%-45s : %6.2f %6s\n", " Soft start resistance (Rss)", C.Rss(2)/1000, "kOhms");

  ## Switching freq
  if !isfield(C, "Rfs")
    C.Rfs       = 2.5e10/P.fsw*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Frequency setting resistor (Rfs)", C.Rfs(2)/1000, "kOhms");

  ## DCR sensing
  if !isfield(C, "Csen")
    error("Csen must be specified")
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense capacitor (Csen)", C.Csen(2)/1e-9, "nF");
  if !isfield(C, "Rsen")
    # Valid for uncoupled inductor only
    C.Rsen      = (PS.Lo.Lk/PS.Lo.dcr)/C.Csen(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense resistor (Rsen)", C.Rsen(2)/1e3, "kOhms");
  if C.Rsen > 5e3
    printf("  --> Rsen too large\n")
  endif

  ## Over current scaling  
  if !isfield(C, "Risen")
    C.Risen     = P.ioc/P.phase*PS.Lo.dcr(2)/Prm.iocavg(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense filter resistor (Risen)", C.Risen(2)/1e3, "kOhms");

  ## Filter capacitor  
  if !isfield(C, "Ct")
    C.Ct          = 27e-9/C.Risen(2)*Ctol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense filter capacitor (Ct)", C.Ct(2)/1e-9, "nF");

  ## Load line
  if !isfield(C, "Rfb")
    C.Rfb       = P.load_line*P.phase*C.Risen(2)/PS.Lo.dcr(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Feedback resistor (Rfb)", C.Rfb(2)/1e3, "kOhms");

  ## Output offset
  if !isfield(C, "Rref")
    C.Rref      = C.Rfb;		# Matching Rfb
  endif
  printf("%-45s : %6.2f %6s\n", " Reference series resistor (Rref)", C.Rref(2)/1e3, "kOhms");
  if !isfield(C, "Rofs")
    if P.Vofs < 0
      C.Rofs    = -Prm.vofsn(2)*C.Rref(2)/P.Vofs*Rtol;
    else
      C.Rofs    = Prm.vofsp(2)*C.Rref(2)/P.Vofs*Rtol;
    endif
  endif
  printf("%-45s : %6.2f %6s\n", " Offset setting resistor (Rofs)", C.Rofs(2)/1e3, "kOhms");

  ## Dynamic VID
  if !isfield(C, "Cref")
    C.Cref      = P.Tvid/C.Rref(2)*Ctol;
  endif
  printf("%-45s : %6.2f %6s\n", " Reference filter capacitor (Cref)", C.Cref(2)/1e-9, "nF");

  ## Enable / disable
  printf("%-45s : %6.2f %6s\n", " Enable pull-up", C.Ruvlo_pu(2)/1e3, "kOhms");
  if !isfield(C, "Ruvlo_pd")
    C.Ruvlo_pd   = Prm.en_pwr_r(2)*C.Ruvlo_pu(2)/(P.uvlo_r-Prm.en_pwr_r(2))*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Enable pull-down", C.Ruvlo_pd(2)/1e3, "kOhms");

  ## Imon
  if !isfield(C, "Rimon")
    C.Rimon   = Prm.imon_oc(2)*P.phase*C.Risen(2)/(PS.Lo.dcr(2)*P.ioc)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current monitoring scaling resistor (Rimon)", C.Rimon(2)/1e3, "kOhms");

  printf("\n");

  ##
  ## Printout actual parameters
  ##
  printf("Achieved parameters:\n")
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");

  ## Switching freq
  P.fsw = 2.5e10*Prm.freq_tol./fliplr(C.Rfs);
  fsw = P.fsw/1000;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");

  ## Output offset
  if C.Rref != C.Rfb
    printf(" Rref must match Rfb!\n");
  endif
  if P.Vofs < 0
    P.Vofs = -Prm.vofsn.*C.Rref./fliplr(C.Rofs);
  else
    P.Vofs = Prm.vofsp.*C.Rref./fliplr(C.Rofs);
  endif
  vofs = P.Vofs*1000;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Nominal offset voltage", vofs(1), vofs(2), vofs(3), "mV");

  ## Nominal output voltage
  P.vout = P.dac.*Prm.ref_tol+P.Vofs;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Output voltage", P.vout(1), P.vout(2), P.vout(3), "V");

  ## Duty cycle estimation
  P.duty = P.vout./fliplr(P.vin);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
         " Duty cycle (ideal)", P.duty(1)*100, P.duty(2)*100, P.duty(3)*100, "%");

  ## DCR sensing matching
  m = ((PS.Lo.Lk./fliplr(PS.Lo.dcr))./fliplr(C.Rsen.*C.Csen) - 1)*100;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " DCR time constant matching", m(1), m(2), m(3), "%");

  ## Load line
  P.load_line = C.Rfb.*PS.Lo.dcr./fliplr(P.phase*C.Risen);
  Rll = P.load_line*1000;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Load line", Rll(1), Rll(2), Rll(3), "mOhm");

  ## Overcurrent setting: compute real inductor current corresponding
  ## to sensed current, neglecting saturation. Consider coupling and
  ## multiphase effect.
  #P.iocppk = C.Risen.*Prm.iocppk.*C.Csen.*C.Rsen./fliplr(PS.Lo.Lk);
  #printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  #	 " Overcurrent (peak per phase)", P.iocppk(1), P.iocppk(2), P.iocppk(3), "A");
  #P.ioc = C.Risen.*Prm.iocavg.*C.Csen.*C.Rsen./fliplr(PS.Lo.Lk)*P.phase;
  #printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  #	 " Overcurrent (peak phase avg)", P.ioc(1), P.ioc(2), P.ioc(3), "A");

  dIL    = P.vout.*(1-P.duty)./fliplr(PS.Lo.Lk.*P.fsw);
  ## Phase average current
  P.iocp_avg = (C.Risen.*Prm.iocppk - fliplr(dIL/2).*(PS.Lo.Lk)./(C.Rsen.*C.Csen))./fliplr(PS.Lo.dcr);
  ## All phase average current
  P.ioc_avg = (C.Risen.*Prm.iocavg - fliplr(dIL/2).*(PS.Lo.Lk)./(P.phase*C.Rsen.*C.Csen))./fliplr(PS.Lo.dcr)*P.phase;
  ## Corresponding peak current  
  if PS.Lo.Cpl == 1
    P.iocp_pk  = P.iocp_avg + dIL/2;
  elseif PS.Lo.Cpl == 2
    P.iocp_pk  = P.iocp_avg + dIL/4;
  else
    error(" --> Coupling value unsupported\n");
  endif
  P.ioc_pk  = P.ioc_avg + dIL/2./P.phase;

  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (per phase, avg)", P.iocp_avg(1), P.iocp_avg(2), P.iocp_avg(3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (per phase, peak)", P.iocp_pk(1), P.iocp_pk(2), P.iocp_pk(3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (total, avg)", P.ioc_avg(1), P.ioc_avg(2), P.ioc_avg(3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (total, peak)", P.ioc_pk(1), P.ioc_pk(2), P.ioc_pk(3), "A");

  ## Isen+ time constant matching
  m = (C.Ct.*C.Risen/27e-9 - 1)*100;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Isen+ time constant matching", m(1), m(2), m(3), "%");

  ## Dynamic Vid
  tc = C.Cref.*C.Rref*1e6;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Dynamic VID time constant", tc(1), tc(2), tc(3), "us");

  ## Enable / disable voltage
  uvlo_r = Prm.en_pwr_r.*(C.Ruvlo_pu+fliplr(C.Ruvlo_pd))./fliplr(C.Ruvlo_pd);
  uvlo_f = Prm.en_pwr_f.*(C.Ruvlo_pu+fliplr(C.Ruvlo_pd))./fliplr(C.Ruvlo_pd);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Enable rising threshold", uvlo_r(1), uvlo_r(2), uvlo_r(3), "V");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Enable falling threshold", uvlo_f(1), uvlo_f(2), uvlo_f(3), "V");

  ## Soft start
  Tss = (1.36e-3 + Prm.Vboot*C.Rss/(25e6*6.25) + 86e-6 + (P.vout-Prm.Vboot).*C.Rss/(25e6*6.25))*1e3;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Soft start time", Tss(1), Tss(2), Tss(3), "ms");

  ## Imon
  ioc = Prm.imon_oc*P.phase.*C.Risen./fliplr(PS.Lo.dcr.*C.Rimon);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " IMON over current", ioc(1), ioc(2), ioc(3), "A");
  gain = P.phase.*C.Risen./fliplr(PS.Lo.dcr.*C.Rimon);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " IMON gain", gain(1), gain(2), gain(3), "A/V");

  printf("\n");

  ##
  ## Control loop (no working for now, work in progress). Use intersil spreadsheet.
  ##
  if 0
    test = 0;

    design.Vin  = 12;
    design.Lout = PS.Lo.Lk(2)/P.phase;
    design.DCR  = PS.Lo.dcr(2)/P.phase;
    design.Cout = PS.Co1.C(2)+PS.Co2.C(2);
    design.ESR  = (PS.Co1.esr(2)*PS.Co1.C(2)+PS.Co2.esr(2)*PS.Co2.C(2))/design.Cout;
    design.Fsw  = P.fsw(2);			# Switcher frequency
    design.DBW  = 75e3;			# 20% to 30% of switcher frequency
    design.R1   = C.Rfb(2);
    design.G    = 1.0;
    design.Fmax = 1e6;
    design.Fmin = 10;
    
    control.Vosc = 1.5;             # From ISL6334 datasheet,
    control.GBW  = 80e6;		# error amp gain bandwidth
    control.DCG  = 96;		# error amp DC gain in dB
    
    comp.R1 = design.R1;
    comp.R2 = 1.91e3;
    comp.R3 = 49.9;
    comp.C1 = 100e-12;
    comp.C2 = 4.7e-9;
    comp.C3 = 470e-12;

    t = "type2";
    if (test == 1)
      [sys, comp] = do_comp(design, control, t, comp);
    else
      [sys, comp] = do_comp(design, control, t);
    endif
    print_comp(t, comp);
    plot_comp(sys, 0, "vcore");
  endif

endfunction
