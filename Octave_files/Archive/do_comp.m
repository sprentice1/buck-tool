# Compute type 3 components and transfer function
#function [g, p, w, comp] = do_comp(design, control, ctype, comp)
function [sys, comp] = do_comp(design, control, ctype, comp)
  Vin  = design.Vin;
  Lout = design.Lout;
  DCR  = design.DCR;
  Cout = design.Cout;
  ESR  = design.ESR;
  Fsw  = design.Fsw;
  DBW  = design.DBW;
  K    = design.K;
  G    = design.G;             # Remote sense gain. Use 1 if direct sense.
  if !exist("comp", "var")
    R1 = design.R1;
  else
    R1 = comp.R1;
    R2 = comp.R2;
    R3 = comp.R3;
    C1 = comp.C1;
    C2 = comp.C2;
    C3 = comp.C3;
  endif

  Vosc = control.Vosc;
  GBW  = control.GBW;
  DCG  = control.DCG;

  ## Compensation of type 2 and 3. This is a mix of TB417/ILS6314/ISL6334/AN-1043
  Flc  = 1 / (2 * pi * sqrt(Lout * Cout))
  Fesr = 1 / (2 * pi * ESR * Cout)

  if Fesr < Flc
    error("Fesr < Flc, unsupported scenario");
  endif
  if (DBW > (Fsw/2))
    warning("DBW > Fsw/2")
  endif

  if ctype == "type2"
    # Z1 placed at K*0.1*Flc
    # P1 placed at Fsw/2
    if !exist("comp", "var")
      if DBW < K*0.1*Flc
        error("Unsupported scenario: DBW < K*0.1*Flc")
      elseif DBW < Flc
        printf("case 1, DBW < Flc\n")
        C1 = Vin/Vosc*G/(R1*pi*Fsw);
        R2 = 1/(2*pi*C1*(0.5*Fsw-K*0.1*Flc));
        C2 = 1/(2*pi*K*0.1*Flc*R2);
      elseif DBW < Fesr
        printf("case 2, DBW > Flc, DBW < Fesr\n")
        C1 = Vin/Vosc*G/(R1*4*pi^3*Lout*Cout*Fsw*DBW^2);
        R2 = 1/(2*pi*C1*(0.5*Fsw-K*0.1*Flc));
        C2 = 1/(2*pi*K*0.1*Flc*R2);
        #R2 = (Vosc/Vin)*(1/G)*R1*Lout*Cout*(2*pi*DBW)^2;
        #C2 = 1/(2*pi*K*0.1*Flc*R2);
        #C1 = C2 / (pi*R2*C2*Fsw - 1);
      else
        printf("case 3, DBW > Fesr\n")
        C1 = Vin/Vosc*G*ESR/(R1*2*pi^2*Lout*Fsw*DBW);
        R2 = 1/(pi*C1*(Fsw-K*0.2*Flc));
        C2 = 1/(2*pi*K*0.1*Flc*R2);
        #R2 = (Vosc/Vin)*(1/G)*R1*(Lout/ESR)*2*pi*DBW;
        #C2 = 1/(2*pi*K*0.1*Flc*R2);
        #C1 = C2 / (pi*R2*C2*Fsw - 1);
      endif
    endif
    comp.R1 = R1;
    comp.R2 = R2;;
    comp.C1 = C1;
    comp.C2 = C2;
  elseif ctype == "type3"
    # Z1 placed at Flc/2
    # Z2 place at K*Flc
    # P1 placed at Fesr
    # P2 placed at Fsw/2
    if !exist("comp", "var")
      if DBW < K*Flc
        printf("case 1, DBW < K*Flc\n")
        C2 = (Vin/Vosc)*G*1/(R1*2*pi*DBW);
        R2 = 1/(pi*Flc*C2);
        C1 = C2 / (2*pi*R2*C2*Fesr - 1);
        R3 = R1 / (Fsw/(2*K*Flc) - 1);
        C3 = 1 / (pi*R3*Fsw);
      elseif DBW < Fesr
        printf("case 2, DBW > Flc, DBW < Fesr\n")
        R2 = (Vosc/Vin)*(1/G)*(DBW/Flc)*R1*K;
        C2 = 1 / (pi*R2*Flc);
        C1 = C2 / (2*pi*R2*C2*Fesr - 1);
        R3 = R1*K / (Fsw/(2*Flc) - 1);
        C3 = 1 / (pi*R3*Fsw);
      else
        printf("case 3, DBW > Fesr\n")
        C3 = 1/(2*pi*R1*Flc*K);
        C1 = (Vin/Vosc)*G*ESR*C3/(Lout*2*pi*DBW);
        R2 = 1/(2*pi*Fesr*C1);
        C2 = 1/(pi*R2*Flc);
        R3 = 1/(pi*Fsw*C3);
      endif
      if (R1 < 10*R3)
        warning("R1 >> R3 not respected")
      endif
      if (C2 < 10*C1)
        warning("C2 >> C1 not respected")
      endif
    endif
    # Returned value
    comp.R1 = R1;
    comp.R2 = R2;;
    comp.R3 = R3;
    comp.C1 = C1;
    comp.C2 = C2;
    comp.C3 = C3;
  else
    printf("No compensation type specified or not supported")
    return
  endif

  ## closed loop system
  ea_n = [10^(DCG/20)];
  ea_d = [sqrt((10^(DCG/20))^2-1) / (2*pi*GBW), 1];
  md_n = Vin;
  md_d = Vosc;
  of_n = [ESR*Cout, 1];
  of_d = [Lout*Cout, Cout*(ESR+DCR), 1];
  if ctype == "type2"
    cp_n = G*[1, 1/(R2*C2)];
    cp_d = (R1*C1)*[1, (C1+C2)/(R2*C1*C2), 0];
  elseif ctype == "type3"
    cp_n = (R1+R3)*G*conv([1, 1/(R2*C2)], [1, 1/((R1+R3)*C3)]);
    cp_d = (R1*R3*C1)*conv([1, (C1+C2)/(R2*C1*C2), 0], [1, 1/(R3*C3)]);
  else
    printf("Compensation type not supported")
    return
  endif

  sys_n = conv(md_n, conv(of_n, cp_n));
  sys_d = conv(md_d, conv(of_d, cp_d));
  sys   = tf(sys_n, sys_d, 0, "in", "out");
endfunction
