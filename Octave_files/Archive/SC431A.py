import numpy as np


class Prm:
    def __init__(self):
        self.vref = np.array([0.594, 0.6, 0.606])
        self.Ton_min = 50e-9
        self.Toff_min = 250e-9


Prm = Prm()
Rtol = np.array([0.99, 1, 1.01])


def SC431A(P, C, PS):
    if P['phase'] != 1:
        print("Number of phase must be one")
        return -1

    D = np.flip(P['vout']/P['vin'])

    print("[=== SC431A component selection ===]\n\n")

    print("Selected components:\n")

    if 'R1' in C and 'R2' not in C:
        if 'R3' in C:
            R1eq = 1.0/(1.0/C['R1'][1] + 1.0/C['R3'][1])
        else:
            R1eq = C['R1'][1]
        C['R2'] = Prm.vref[1]*R1eq/(Prm.vref[1])
    elif 'R1' not in C and 'R2' in C:
        R1eq = (P['vout'] - Prm.vref[1])*C['R2'][1]/(Prm.vref[1])
        if 'R3' in C:
            C['R1'] = R1eq*C['R3'][1]/(C['R3'][1] - R1eq)*Rtol
        else:
            C['R1'] = R1eq*Rtol
    elif 'R1' not in C and 'R2' not in C:
        print("R1 or R2 must be provided")
        return -1

    if 'R3' in C:
        C['R1'] = C['R1']*C['R3']/(C['R1'] + C['R3'])

    print("{:45s} : {:6.2f} {:6s}\n".format(" Voltage setting resistor (R1)", C['R1'][1] / 1e3, "kOhms"))
    print("{:45s} : {:6.2f} {:6s}\n".format(" Voltage setting resistor (R2)", C['R2'][1] / 1e3, "kOhms"))

    P['vout'] = Prm.vref/(np.flip(C['R2'])/(C['R1'] + np.flip(C['R2'])))

    Ton = (P['vout']/(P['fsw']*P['vin']))*np.array([0.9, 1, 1.1])
    Toff = ((1.0-D)/P['fsw'])*[0.9, 1, 1.1]
    P['fsw'] = P['vout']/(Ton*P['vin'])
    P['fsw'] = np.flip(P['fsw'])

    Icin = P['iout']*np.sqrt(D*(1-D)+(1/12)*((P['vout']/(PS['Lo'].Lk*P['fsw']*P['iout'])**2)*(((1-D)**2)*D)))

    K = ((P['vin']-P['vout'])*D)/(PS['Lo'].Lk*P['iout']*P['fsw'])
    ir = Ton*(P['vin'] - P['vout'])/np.flip(PS['Lo'].Lk)

    R = PS['Co'].esr
    C = PS['Co'].C

    Cout_min = (PS['Lo'].Lk*(P['iout']+0.5*K*P['iout'])**2)/(P['vpeak']**2 - P['vout']**2)

    Vout_min = -(ir * R ** 2 * C / Ton) - (ir / (2 * C * Ton)) * ((Ton / 2) ** 2 - (R * C) ** 2)
    Vout_max = ir * R ** 2 * C / Toff + (ir / (2 * C * Toff)) * ((Toff / 2) ** 2 - (R * C) ** 2)

    Cin_min = P['iout'] * (D * (1 - D)) / ((P['vout'][2] - P['vout'][0]) * np.flip(P['fsw']))
    vout_ripple = np.array([0, 0, 0])

    if ((Ton / 2) - R * C).any() > 0 and ((Toff / 2) - R * C).any() > 0:
        vout_ripple = Vout_max - Vout_min
        vout_ripple = np.flip(vout_ripple)

    elif (R * C > Ton / 2).any() and (R * C > Toff).any() / 2:
        vout_ripple = ir * R
        vout_ripple = np.flip(vout_ripple)

    elif (R * C > Ton / 2).any() and (R * C < Toff).any() / 2:
        vout_ripple = Vout_max + ir * R / 2
        vout_ripple = np.flip(vout_ripple)

    elif (R * C < Ton / 2).any() and (R * C > Toff).any() / 2:
        vout_ripple = -Vout_min + ir * R / 2
        vout_ripple = np.flip(vout_ripple)

    fsw = P['fsw']/1e3

    P['Tss'] = [1.8e-3, 3e-3, 4.2e-3]

    print("{:35s} : {:5.1f}, {:5.1f}, {:5.1f} {:5s}".format(
        " Switching frequency", P['fsw'][0], P['fsw'][1], P['fsw'][2], "kHz"))

    print("{:35s} : {:5.1f}, {:5.1f}, {:5.1f} {:5s}".format(
           " Ton", Ton[0] * 1e9, Ton[1] * 1e9, Ton[2] * 1e9, "ns"))

    print("{:35s} : {:5.1f}, {:5.1f}, {:5.1f} {:5s}".format(
           " Toff", Toff[0] * 1e9, Toff[1] * 1e9, Toff[2] * 1e9, "ns"))

    print("{:35s} : {:5.1f}, {:5.1f}, {:5.1f} {:5s}".format(
           " Switching frequency", fsw[0], fsw[1], fsw[2], "kHz"))

    print("{:35s} : {:5.3f}, {:5.3f}, {:5.3f} {:5s}".format(
           " Vout (DC)", P['vout'][0], P['vout'][1], P['vout'][2], "V"))

    print("{:35s} : {:5.3f}, {:5.3f}, {:5.3f} {:5s}".format(
           " Recommended Cout for 1us load step", Cout_min[0] * 1e6, Cout_min[1] * 1e6, Cout_min[2] * 1e6, "uF"))

    print("{:35s} : {:5.3f}, {:5.3f}, {:5.3f} {:5s}".format(
           " Recommended minimum Cin", Cin_min[0] * 1e6, Cin_min[1] * 1e6, Cin_min[2] * 1e6, "uF"))

    print("{:35s} : {:5.3f}, {:5.3f}, {:5.3f} {:5s}".format(
           " Output ripple voltage", vout_ripple[0] * 1000, vout_ripple[1] * 1000, vout_ripple[2] * 1000, "mV"))

    print("{:35s} : {:5.3f}, {:5.3f}, {:5.3f} {:5s}".format(
           " Output ripple Current", ir[0], ir[1], ir[2], "A"))

    print("{:35s} : {:5.3f}, {:5.3f}, {:5.3f} {:5s}".format(
           " Input capacitor Current", Icin[0], Icin[1], Icin[2], "A"))

    return P, C




