# Compute components from targets or compute
# achieved parameters versus targets using supplied components
# values.
#
# P   : Target parameters
# C   : Control components selection
# PS  : Power stage components selection
# name: name for plot
#
# TODO: 
#       Modify compensation to design for worst case and plot
#       typ, min, max (monte carlo ?)
#       Consider DAC accuracy variation for Vout computation
#       -Over current still not exact, need to correct effect of
#        multiphase ripple
#
function [P, C] = ISL6308A(P, C, PS, name)
  # Chip parameters
  Prm.ref_tol  = [0.995, 1, 1.005];
  Prm.en_pwr_r = [0.595, 0.630, 0.670];
  Prm.en_pwr_f = Prm.en_pwr_r - fliplr([0.028, 0.035, 0.043]);
  Prm.vofsp    = [47.5, 50, 52.5]*1e-6*10e3;
  Prm.vofsn    = [47.5, 50, 52.5]*1e-6*30e3;
  Prm.ioc      = [93, 100, 107]*1e-6;
  Prm.freq_tol = [0.9, 1, 1.1];
  Prm.Dmin     = 0;
  Prm.Dmax     = 2/3;

  Ctol = [0.95, 1, 1.05];
  Rtol = [0.99, 1, 1.01];

  ## 
  ## Components selection
  ## 
  printf("=== ISL6308A component selection ===\n\n");

  if P.phase > 3
    error("Number of phase must be 3 or less")
  endif

  printf("Selected components:\n\n")

  ## DAC setting is per user
  if !isfield(P, "dac")
    error("DAC setting needs to be provided")
  else
    if P.dac != 0.6 && P.dac != 0.9 && P.dac != 1.2 && P.dac != 1.5
      printf(" --> Invalid DAC setting\n")
    endif
  endif
  printf("%-45s : %6.2f %6s\n", " Selected DAC setting", P.dac, "Volt");

  ## Rfb must be selected
  if !isfield(C, "Rfb")
    error("Rfb must be specified")
  endif
  printf("%-45s : %6.2f %6s\n", " Feedback resistor (Rfb)", C.Rfb(2)/1e3, "kOhms");

  ## Switching freq
  if !isfield(C, "Rfs")
    C.Rfs       = 10^(10.61-1.035*log10(P.fsw))*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Frequency setting resistor (Rfs)", C.Rfs(2)/1000, "kOhms");

  ## DCR sensing
  if !isfield(C, "Ccomp")
    error("Ccomp must be specified")
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense capacitor (Ccomp)", C.Ccomp(2)/1e-9, "nF");
  if !isfield(C, "Rcomp")
    C.Rcomp     = (PS.Lo.Lk(2)/PS.Lo.dcr(2))/C.Ccomp(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense resistor (Rcomp)", C.Rcomp(2)/1e3, "kOhms");

  ## Current balance sensing
  if !isfield(C, "Risen")
    C.Risen     = PS.Bf.Rds(2)*max(P.iout)/P.phase/50e-6.*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current balance resistor (Risen)", C.Risen(2)/1e3, "kOhms");

  ## Load line, even if not used this is part of over current
  if !isfield(C, "Rs")
    C.Rs        = C.Rcomp(2)/P.load_line*PS.Lo.dcr(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense scaling resistor (Rs)", C.Rs(2)/1e3, "kOhms");
  if C.Rs(1) < 12e3
    printf(" --> Rs too small, select smaller Ccomp\n")
  endif

  ## Overcurrent
  if !isfield(C, "Rocset")
    C.Rocset    = P.ioc*PS.Lo.dcr(2)*C.Rcomp(2)/(Prm.ioc(2)*C.Rs(2))*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Over current setting resistor (Rocset)", C.Rocset(2)/1e3, "kOhms");

  ## Output offset
  if !isfield(C, "Rofs")
    if P.Vofs < 0
      C.Rofs    = -Prm.vofsn(2)*C.Rfb(2)/P.Vofs*Rtol;
    elseif P.Vofs > 0
      C.Rofs    = Prm.vofsp(2)*C.Rfb(2)/P.Vofs*Rtol;
    else
      C.Rofs    = [Inf, Inf, Inf];
    endif
  endif
  printf("%-45s : %6.2f %6s\n", " Offset setting resistor (Rofs)", C.Rofs(2)/1e3, "kOhms");

  ## Enable / disable
  if isfield(C, "Ruvlo_pu")
    printf("%-45s : %6.2f %6s\n", " Enable pull-up", C.Ruvlo_pu(2)/1e3, "kOhms");
    if !isfield(C, "Ruvlo_pd")
      C.Ruvlo_pd   = Prm.en_pwr_r(2)*C.Ruvlo_pu(2)/(P.uvlo_r-Prm.en_pwr_r(2))*Rtol;
    endif
    printf("%-45s : %6.2f %6s\n", " Enable pull-down", C.Ruvlo_pd(2)/1e3, "kOhms");
  endif

  printf("\n");

  ##
  ## Compute and printout achieved parameters
  ##
  printf("Achieved parameters:\n")
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");

  ## Switching freq
  P.fsw = 10.^(10.25 - 0.966*log10(fliplr(C.Rfs))).*Prm.freq_tol;
  fsw = P.fsw/1000;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");

  ## Output offset
  if P.Vofs < 0
    P.Vofs = -Prm.vofsn.*C.Rfb./fliplr(C.Rofs);
  else
    P.Vofs = Prm.vofsp.*C.Rfb./fliplr(C.Rofs);
  endif
  vofs = P.Vofs*1000;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Nominal offset voltage", vofs(1), vofs(2), vofs(3), "mV");

  ## Nominal output
  P.vout = P.dac.*Prm.ref_tol+P.Vofs;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Output voltage", P.vout(1), P.vout(2), P.vout(3), "V");

  ## Duty cycle limitation (approximation, no losses)
  P.duty = P.vout./fliplr(P.vin);
  if P.duty < Prm.Dmin || P.duty > Prm.Dmax
    error("Duty cycle limitation")
  endif
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
         " Duty cycle (ideal)", P.duty(1)*100, P.duty(2)*100, P.duty(3)*100, "%");

  ## DCR sensing matching
  m = ((PS.Lo.Lk./fliplr(PS.Lo.dcr))./fliplr(C.Rcomp.*C.Ccomp) - 1)*100;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " DCR time constant matching", m(1), m(2), m(3), "%");

  ## Load line
  P.load_line = C.Rcomp.*PS.Lo.dcr./fliplr(C.Rs);
  Rll = P.load_line*1000;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Load line", Rll(1), Rll(2), Rll(3), "mOhm");

  ## Overcurrent setting
  #P.ioc = C.Rocset.*Prm.ioc.*C.Rs./fliplr(C.Rcomp.*PS.Lo.dcr); #Perfect DCR macthing
  #P.ioc = C.Rocset.*Prm.ioc.*C.Rs.*C.Ccomp./fliplr(PS.Lo.Lk);  #Considereing DCR matching, no DC
  #printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  #     "Overcurrent (peak phases avg)", P.ioc(1), P.ioc(2), P.ioc(3), "A");
  if PS.Lo.Cpl != 1
    printf(" --> Coupled inductor not supported\n");
  endif
  dIL = P.vout.*(1-P.duty)./fliplr(PS.Lo.Lk.*P.fsw*P.phase);
  P.ioc_avg = (C.Rocset.*Prm.ioc.*C.Rs - dIL/2.*fliplr(PS.Lo.Lk)./C.Ccomp)./fliplr(C.Rcomp.*PS.Lo.dcr);
  P.ioc_pk  = P.ioc_avg + dIL/2;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Overcurrent (mean)", P.ioc_avg(1), P.ioc_avg(2), P.ioc_avg(3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Overcurrent (peak)", P.ioc_pk(1), P.ioc_pk(2), P.ioc_pk(3), "A");

  ## Enable / disable voltage
  if isfield(C, "Ruvlo_pu")
    uvlo_r = Prm.en_pwr_r.*(C.Ruvlo_pu+fliplr(C.Ruvlo_pd))./fliplr(C.Ruvlo_pd);
    uvlo_f = Prm.en_pwr_f.*(C.Ruvlo_pu+fliplr(C.Ruvlo_pd))./fliplr(C.Ruvlo_pd);
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   " Enable rising threshold", uvlo_r(1), uvlo_r(2), uvlo_r(3), "V");
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   " Enable falling threshold", uvlo_f(1), uvlo_f(2), uvlo_f(3), "V");
    printf("\n");
  endif

  printf("\n");

  ## 
  ## Compensation design
  ##
  printf("=== ISL6308A compensation design ===\n\n");
  if !isfield(C, "R1")
    warning("R1 not specified, compensation skipped\n")
  else
    if !isfield(C, "R2") | !isfield(C, "R3") | !isfield(C, "C1") | \
          !isfield(C, "C2") | !isfield(C, "C3")
      dotest = 0;
    else
      dotest = 1;
    endif

    design.R1   = C.R1(2);
    design.Vin  = P.vin(3);     # Design stability for maximum voltage voltage
    design.Lout = PS.Lo.Lk(2)/P.phase;
    design.DCR  = PS.Lo.dcr(2)/P.phase;
    Ceq         = 1./sum(1./(PS.Co.esr + 1./(2*pi*P.DBW*PS.Co.C*i)), 1); # effective at DBW
    design.Cout = -1./(2*pi*P.DBW*imag(Ceq(2)));
    design.ESR  = real(Ceq(2));
    design.Fsw  = P.fsw(2);
    design.G    = 1.0;          # Remote sense amplifier gain. If ever a
                                # voltage dividor is used, this must be changed.
    design.Fmax = 1e6;
    design.Fmin = 10;

    control.Vosc = 1.5/Prm.Dmax;# From ISL6308A datasheet,
    control.GBW  = 20e6;        # error amp gain bandwidth
    control.DCG  = 96;		# error amp DC gain in dB
    if !isfield(P, "DBW")
      design.DBW = P.fsw(1)/5;
    else
      design.DBW = P.DBW;
      if design.DBW > P.fsw(1)/3
        warning("Converter bandwidth must be lower than Fsw/3\n")
      endif
    endif
    design.K     = P.K;

    if P.Type == 3
      if dotest == 0
        [sys, comp] = do_comp(design, control, "type3");
      else
        comp.R1 = C.R1(2);
        comp.R2 = C.R2(2);
        comp.R3 = C.R3(2);
        comp.C1 = C.C1(2);
        comp.C2 = C.C2(2);
        comp.C3 = C.C3(2);
        [sys, comp] = do_comp(design, control, "type3", comp);
      endif
    else
      error("To do")
    endif
    print_comp("type3", comp);
    plot_comp(sys, 1, name);

    printf("\n");
  endif
endfunction
