function [g, p, w] = plot_comp(sys, to_ps, prefix, Fmin, Fmax)
  if !exist("Fmin", "var")
    Fmin=10;
    Fmax=1e6;
  else
    if !exist("Fmax", "var")
      Fmax=1e6;
    endif
  endif

  # Evaluate bode plot
  f = logspace(log10(Fmin), log10(Fmax), 1000);
  w = 2*pi*f;
  [g, p, w] = bode(sys, w);

  if !ishold()
    ## Find cross over and phase margin
    a=0.5;
    while a > 1e-4
      idx = find(g > (1-a) & g < (1+a));
      if length(idx) == 0
        warning("No cross over found")
        idx=1;
        break
      elseif length(idx) <= 2
        break
      else
        a = a/2;
      endif
    endwhile
    Fc = w(idx(1))/(2*pi);
    printf("Cross over  : %.2f kHz\n", Fc/1000);
    Pm = p(idx(1));
    printf("Phase margin: %.f degres\n", Pm+180);
    semilogx(f, 20*log10(g), ";Closed loop gain;",
             f, p, ";Closed loop phase;",
             Fc, 0, "xr",
             Fc, Pm, "xr");
    text(Fmin*2, -135, sprintf("Fc : %.2f kHz\nPm : %.f degres", Fc/1000, Pm+180))
  else
    semilogx(f, 20*log10(g),
             f, p)
  endif

  grid("on")
  title(sprintf("Compensation for %s", prefix));
  xlabel("Frequency (Hz)");
  ylabel("Gain (dB) / Phase (degres)");

  if (to_ps)
    print(sprintf("%s.png", prefix), "-dpng")
  endif
endfunction
