# Compute components from targets or compute
# achieved parameters with supplied components values
#
# P   : Target parameters
#       P.vin
#       P.vout
#       P.iout
# C   : Control components selection
#       C.Rfb1
#       C.Rfb2
# PS  : Power stage components selection
#
function [P, C] = AOZ1022(P, C, PS, name)
  # Chip parameters
  Prm.vref     = 0.8*[0.985, 1, 1.015];
  Prm.ifb      = [0, 20, 200]*1e-9;               # min/typ guessed
  Prm.vpgl     = [0.87, 0.9, 0.92];
  Prm.vpgh     = [0.03];                          # only typical provided
  Prm.ilim     = [4, 4.5, 5];                     # typical guessed
  Prm.tss      = [3, 5, 6]*1e-3;
  Prm.Dmax     = 1;
  Prm.Dmin     = 0.06;
  Prm.fsw      = [350, 500, 600]*1e3;
  Prm.Gea      = 140e-6;
  Prm.Gvea     = 500;
  Prm.Gcs      = 6.86;
  
  Ctol = [0.95, 1, 1.05];
  Rtol = [0.99, 1, 1.01];

  ## 
  ## Components selection
  ## 
  if P.phase != 1
    error("Number of phase must be one")
  endif

  printf("=== AOZ1022D component selection ===\n\n");

  printf("Selected components:\n")

  ## Output voltage
  if isfield(C, "Rfb1") & !isfield(C, "Rfb2")
    C.Rfb2 = Prm.vref(2)*C.Rfb1(2)/(P.vout-Prm.vref(2)-Prm.ifb(2)*C.Rfb1(2))*Rtol;
  elseif !isfield(C, "Rfb1") & isfield(C, "Rfb2")
    C.Rfb1 = (P.vout - Prm.vref(2))*C.Rfb2(2)./(Prm.ifb(2)*C.Rfb2(2)+Prm.vref(2))*Rtol;
  elseif !isfield(C, "Rfb1") & !isfield(C, "Rfb2")
    error("Rfb1 or Rfb2 must be provided")
  endif
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (Rfb1)", C.Rfb1(2)/1e3, "kOhms");
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (Rfb2)", C.Rfb2(2)/1e3, "kOhms");

  printf("\n");

  ##
  ## Compute and printout achieved parameters
  ##
  printf("Achieved parameters:\n")
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");

  ## Switching frequency
  P.fsw = Prm.fsw;
  fsw   = P.fsw/1e3;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");
  ## output voltage
  P.vout = Prm.vref.*C.Rfb1./fliplr(C.Rfb2)+Prm.vref+Prm.ifb.*C.Rfb1;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Output voltage", P.vout(1), P.vout(2), P.vout(3), "V");
  ## Duty cycle limitation (approximation, no losses)
  P.duty = P.vout./fliplr(P.vin);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Duty cycle", P.duty(1)*100, P.duty(2)*100, P.duty(3)*100, "%");
  if P.duty < Prm.Dmin || P.duty > Prm.Dmax
    printf(" --> Duty cycle limitation")
  endif
  ## overcurrent
  P.ioc_pk = Prm.ilim;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Overcurrent (peak, no heating)", Prm.ilim(1), Prm.ilim(2), Prm.ilim(3), "A");
  ## overcurrent (mean, approximation, no losses)
  dIL = P.vout.*(1-P.duty)./fliplr(PS.Lo.Lk.*P.fsw);
  P.ioc_avg = P.ioc_pk - fliplr(dIL/2);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Overcurrent (average, no heating)", P.ioc_avg(1), P.ioc_avg(2), P.ioc_avg(3), "A");
  ## soft start
  Tss   = Prm.tss*1e3;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Soft start", Tss(1), Tss(2), Tss(3), "ms");
  ## Power good
  P.Vpwrdgh = P.vout.*(Prm.vpgl+Prm.vpgh);
  P.Vpwrdgl = P.vout.*Prm.vpgl;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Power good rising threshold", P.Vpwrdgh(1), P.Vpwrdgh(2), P.Vpwrdgh(3), "V");
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Power good falling threshold", P.Vpwrdgl(1), P.Vpwrdgl(2), P.Vpwrdgl(3), "V");

  printf("\n");

  ##
  ## Compensation design to be impemented
  ##
  R1 = C.Rfb1;
  R2 = C.Rfb2;
  RL = P.vout(2)/P.iout(3)*10;   # compensate at 10% of Imax
  Co = PS.Co.C(2);
  Esr = PS.Co.esr(2);
  Fp1 = 1/(2*pi*Co*RL);
  Fz1 = 1/(2*pi*Co*Esr);

  if !isfield(C, "Rc") || !isfield(C, "Cc")
    Fc  = Prm.fsw(1)/10;
    Rc  = Fc*2*pi*P.vout(2)*PS.Co.C(2)/(Prm.vref(2)*Prm.Gea*Prm.Gcs);
    Cc  = 1/(2*pi*Rc*Fp1);
  else
    Rc = C.Rc;
    Cc = C.Cc;
  endif

  Ramp = Prm.Gvea/Prm.Gea;
  Fp2 = 1/(2*pi*Ramp*Cc);
  Fz2 = 1/(2*pi*Rc*Cc);

  printf("Compensation:\n")
  printf("%-35s : %5.1f %-5s\n", " Rc", Rc/1000, "kOhms");
  printf("%-35s : %5.1f %-5s\n", " Cc", Cc*1e9, "nF");
  printf("%-35s : %5.0f %-5s\n", " Fp1", Fp1, "Hz");
  printf("%-35s : %5.0f %-5s\n", " Fp2", Fp2, "Hz");
  printf("%-35s : %5.2f %-5s\n", " Fz1", Fz1/1e6, "MHz");
  printf("%-35s : %5.0f %-5s\n", " Fz2", Fz2, "Hz");

  ## closed loop system
  idx = 1;
  for RL = P.vout(2)./P.iout
    cp_n = R2/(R1+R2)*Prm.Gvea*[Rc*Cc, 1];
    cp_d = [(Ramp+Rc)*Cc, 1];
    of_n = Prm.Gcs*RL*[Co*Esr, 1];
    of_d = [(RL+Esr)*Co, 1];
    
    sys_n = conv(cp_n, of_n);
    sys_d = conv(cp_d, of_d);
    sys   = tf(sys_n, sys_d, 0, "in", "out");

    if idx < length(P.iout)
      [g, p, w] = plot_comp(sys, 0, name);
      hold on
    else
      [g, p, w] = plot_comp(sys, 1, name);
      hold off
    endif

    idx = idx + 1;
  endfor
  printf("\n")
endfunction
