# Design equation for V_1V05 based on SC402B

clear all

source("../lib/Mosfets.m");
source("../lib/GateDrv.m");
source("../lib/Capacitors.m");
source("../lib/Inductors.m");
addpath("../lib");

Ctol = [0.85, 1, 1.15];
Rtol = [0.99, 1, 1.01];
Rtol2= [0.999, 1, 1.001];

# Converter parameters and requirements
P.vin       = 12*[0.95, 1, 1.05];
P.vout      = 1.00;
P.iout      = [0.01, 3, 5];
P.ioc       = 6;
P.load_line = [0,0,0];
P.phase     = 1;
P.fsw       = 533.333e3;
P.t         = [0, 25, 70];          # Operating temperature
P.Gv        = 12;                   # Gate voltage

# Controller selected parameter
C.R1        = 1.3e3*Rtol2;
C.Css       = 10e-9*Ctol;

# Computed and then selected parameters
C.R2        = 1.831e3*Rtol2;
C.R3        = 127e3*Rtol;
#C.C1        = 680e-12*Ctol;
C.Rton      = 75e3*Rtol;
C.Rilim     = 4.7e3*Rtol;

#
# power stage components
#
# Input filter
PS.Li       = Inductors_get(L_80n_6A_4mR_1206(0, P.t), 1);
PS.Ci       = Capacitors_get(C_10u_25V_X5R_1206(P.vin(2), P.t), 2);
# Output filter
PS.Lo       = Inductors_get(L_470n_17A_4mR_2525(0, P.t), 1);
PS.Co       = Capacitors_get(C_560u_2V_POSCAP_7343(P.vout, P.t), 3,C_47u_4V_X6S_0805(P.vout, P.t),3);

# MOSFET
[PS.Tf, PS.Bf]     = M_SC417(P.Gv, P.t);
PS.D               = PS.Bf.D;
[PS.Tfdr, PS.Bfdr] = GateDrv_SC417(P.Gv, P.t);

# Run control component selection
[P, C] = SC402B(P, C, PS, "v1v05");
# Run power stage computation
R = Pstage(P, PS);
