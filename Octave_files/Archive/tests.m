# Some  tests
ctype = "type2"

# LC filter exact function
function G = Gfe(s, Lo, Co, Rdcr, Resr)
  G = (Resr*Co*s+1)./(Lo*Co*s.^2+Co*(Resr+Rdcr)*s+1);
endfunction

# LC function graphical approximation
# Gain = 1, below Flc
# Gain = 1/(Lo*Co*s^2) between Flc & Fesr
# Gain = Resr/(Lo*s) above Fesr
function G = Gfa(s, Lo, Co, Rdcr, Resr)
  sFlc = 1/(sqrt(Lo*Co));
  sFesr = 1/(Resr*Co);

  G = (imag(s) < sFlc)*1 + \
      (imag(s) > sFlc & imag(s) < sFesr).*1./(Lo*Co*s.^2) + \
      (imag(s) > sFesr).*Resr./(Lo*s);
endfunction

# Modulator exact function
function G = Gme(s, R1, C1, R2, C2, R3, C3, ctype)
  if ctype == "type2"
    G = (R2*C2*s+1)./(R1*s.*(R2*C1*C2*s+C1+C2));
  elseif ctype == "type3"
    G = (R2*C2*s+1).*((R1+R3)*C3*s+1)./(R1*s.*(R2*C1*C2*s+C1+C2).*(R3*C3*s+1));
  else
    error("bad type specified")
  endif
endfunction

# Modulator function graphical approximation
# Type 2:
# Gain = 1/(R1*(C1+C2)*s), below Z1
# Gain = R2*C2/(R1*(C1+C2)), between Z1 & P1
# Gain = 1/(R1*C1*s), above P1
# Type 3:
# Gain = 1/(R1*(C1+C2)*s), below Z1
# Gain = R2*C2/(R1*C1+C2), between Z1 & Z2
# Gain = C2*C3*R2*(R1+R3)/(R1*(C1+C2))*s, between Z2 & P1
# Gain = C3*(R1+R3)/(C1*R1), between P1 & P2
# Gain = (R1+R3)/(R1*R3*C1*s), above P2
function G = Gma(s, R1, C1, R2, C2, R3, C3, ctype)
  if ctype == "type2"
    Z1 = 1/(R2*C2);
    P1 = (C1+C2)/(R2*C1*C2);
    
    G = (imag(s) < Z1)./(R1*(C1+C2).*s) + \
        (imag(s) > Z1 & imag(s) < P1)*R2*C2/(R1*(C1+C2)) + \
        (imag(s) > P1).*1./(R1*C1*s);
  elseif ctype == "type3"
    Z1 = 1/(R2*C2);
    Z2 = 1/((R1+R3)*C3);
    P1 = (C1+C2)/(R2*C1*C2);
    P2 = 1/(R3*C3);
    
    G = (imag(s) < Z1)./(R1*(C1+C2)*s) + \
        (imag(s) > Z1 & imag(s) < Z2)*R2*C2/(R1*(C1+C2)) + \
        (imag(s) > Z2 & imag(s) < P1).*C2*C3*R2*(R1+R3)/(R1*(C1+C2)).*s + \
        (imag(s) > P1 & imag(s) < P2)*C3*(R1+R3)/(C1*R1) + \
        (imag(s) > P2).*(R1+R3)./(R1*R3*C1*s);
  else
    error("bad type specified")
  endif
endfunction

####
design.Vin   = 5;
design.Lout  = 0.47e-6;
design.DCR   = 4e-3;
design.Cout  = 470e-6;
design.ESR   = 15e-3;
design.Fsw   = 800e3;
design.G     = 1.0;
design.Fmax  = 1e7;
design.Fmin  = 10;

control.Vosc = 1.5;           # From ISL6314 datasheet,
control.GBW  = 40e6;		# error amp gain bandwidth
control.DCG  = 96;		# error amp DC gain in dB
design.DBW   = 100e3;;
design.K     = 2;
design.R1    = 1e3;

[sys, comp] = do_comp(design, control, ctype);
print_comp(ctype, comp);
[g, p, w] = plot_comp(sys, 0, "test", design.Fmin, design.Fmax);
pause

####
f = logspace(log10(design.Fmin), log10(design.Fmax), 1000);
s = 2*pi*f*j;

Lo   = design.Lout;
Co   = design.Cout;
Rdcr = design.DCR;
Resr = design.ESR;

R1 = comp.R1;
C1 = comp.C1;
R2 = comp.R2;
C2 = comp.C2;
if ctype == "type2"
  R3 = 0;
  C3 = 0;
elseif ctype == "type3"
  R3 = comp.R3;
  C3 = comp.C3;
else
  error("bad compensation type specified")
endif

G1 = Gfe(s, Lo, Co, Rdcr, Resr);
G2 = Gfa(s, Lo, Co, Rdcr, Resr);
semilogx (f, 20*log10(abs(G1)),
          f, 180/pi*(arg(G1)),
          f, 20*log10(abs(G2)),
          f, 180/pi*(arg(G2)))
pause
G3 = Gme(s, R1, C1, R2, C2, R3, C3, ctype);
G4 = Gma(s, R1, C1, R2, C2, R3, C3, ctype);

semilogx (f, 20*log10(abs(G3)),
          f, 180/pi*(arg(G3)),f,
          20*log10(abs(G4)),
          f, 180/pi*(arg(G4)))
pause
semilogx (f, 20*log10(abs(G2)), f, 180/pi*(arg(G2)),
          f, 20*log10(abs(G4)), f, 180/pi*(arg(G4)))