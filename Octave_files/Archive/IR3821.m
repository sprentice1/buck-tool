# Compute components from targets or compute
# achieved parameters supplied components values
#
# P   : Target parameters
# C   : Control components selection
# PS  : Power stage components selection
#
function [P, C] = IR3821(P, C, PS, name)
  # Chip parameters
  Prm.vref     = 0.6*[0.985, 1, 1.015];
  Prm.ifb      = [-00.5, -0.1, 0.0]*1e-6;
  Prm.iss      = [15, 20, 28]*1e-6;               # soft start current
  Prm.vsnsl    = [0.35, 0.38, 0.41];              # lower trip point
  Prm.vsnsh    = Prm.vsnsl + [15, 27.5, 40]*1e-3; # upper trip point
  Prm.isns     = [0, 0.3, 1]*1e-6;                # Current into Vsns pin
  Prm.ioc      = [15, 20, 26]*1e-6;
  Prm.fsw      = [540, 600, 660]*1e3;
  Prm.rdson    = [7.6, 10.5, 13.4*1.5]*1e-3;          # Typ at 25oC,
                                                      # 12V, max at 100oC
  Prm.rdson    = [7.6, 10.5, 13.4*1.5]*1e-3*1.53;     # At 5V Vgs
  
  Ctol = [0.95, 1, 1.05];
  Rtol = [0.99, 1, 1.01];

  ## 
  ## Components selection
  ## 

  if P.phase != 1
    error("Number of phase must be one")
  endif

  printf("Selected components:\n")

  ## Switching freq is not selectable
  if isfield(C, "Rfs")
    warning("Switching frequency is not adjustable")
  endif
  ## Overcurrent, low side FET sensing
  if !isfield(C, "Rocset")
    C.Rocset    = P.ioc*Prm.rdson(2)/Prm.ioc(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", "Overcurrent setting (Rocset)", C.Rocset(2)/1000, "kOhms");  
  ## Soft start
  if !isfield(C, "Tss")
    C.Css       = Prm.iss(2)*P.Tss*Ctol;
  endif
  printf("%-45s : %6.2f %6s\n", "Soft start capacitor (Css)", C.Css(2)/1e-6, "uF");
  ## Output voltage
  if !isfield(C, "R4") & isfield(C, "R1")
    C.R4 = Prm.vref(2)*C.R1(2)/(P.vout-Prm.vref(2)-Prm.ifb(2)*C.R1(2))*Rtol;
  elseif isfield(C, "R4") & !isfield(C, "R1")
    C.R1 = (P.vout - Prm.vref(2))*C.R4(2)./(Prm.ifb(2)*C.R4(2)+Prm.vref(2))*Rtol;
  elseif !isfield(C, "R4") & !isfield(C, "R1")
    error("R1 or R4 must be provided")
  endif
  printf("%-45s : %6.2f %6s\n", "Voltage setting resistor (R1)", C.R1(2)/1e3, "kOhms");
  printf("%-45s : %6.2f %6s\n", "Voltage setting resistor (R4)", C.R4(2)/1e3, "kOhms");
  ## Power good
  if !isfield(C, "Rsns1") & isfield(C, "Rsns2")
    C.Rsns1 = (P.Vpwrgd - Prm.vsnsh(2))*C.Rsns2(2)/(C.Rsns2(2)*Prm.isns(2)+Prm.vsnsh(2))*Rtol;
  elseif isfield(C, "Rsns1") & !isfield(C, "Rsns2")
    C.Rsns2 = Prm.vsnsh(2)*C.Rsns1(2)/(P.Vpwrgd - Prm.vsnsh(2) - C.Rsns1(2)*Prm.isns(2))*Rtol;
  elseif !isfield(C, "Rsns1") & !isfield(C, "Rsns2")
    error("Rsns1 or Rsns2 must be selected")
  endif
  #if !isfield(C, "Rsns2")
  #  C.Rsns2 = (P.Vpwrgd./Prm.vsnsh(2) - 1).*C.Rsns1(2).*Rtol;
  #endif
  printf("%-45s : %6.2f %6s\n", "Power good threshold resistor (Rsns1)", C.Rsns1(2)/1e3, "kOhms");
  printf("%-45s : %6.2f %6s\n", "Power good threshold resistor (Rsns2)", C.Rsns2(2)/1e3, "kOhms");

  printf("\n");

  ##
  ## Compute and printout achieved parameters
  ##
  printf("Achieved parameters:\n")
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");

  ## Switching frequency
  P.fsw = Prm.fsw;
  fsw   = P.fsw/1e3;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");
  ## overcurrent
  P.ioc = C.Rocset.*Prm.ioc./fliplr(Prm.rdson);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Overcurrent (peak, no heating)", P.ioc(1), P.ioc(2), P.ioc(3), "A");
  ## soft start
  P.Tss = C.Css ./ fliplr(Prm.iss);
  Tss   = P.Tss*1e3;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 "Soft start", Tss(1), Tss(2), Tss(3), "ms");
  ## output voltage
  P.vout = Prm.vref.*C.R1./fliplr(C.R4)+Prm.vref+Prm.ifb.*C.R1;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 "Output voltage", P.vout(1), P.vout(2), P.vout(3), "V");
  ## Power good
  P.Vpwrdgh = Prm.vsnsh.*(C.Rsns1./fliplr(C.Rsns2)+1) + Prm.isns.*C.Rsns1;
  P.Vpwrdgl = Prm.vsnsl.*(C.Rsns1./fliplr(C.Rsns2)+1) + Prm.isns.*C.Rsns1;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 "Power good rising threshold", P.Vpwrdgh(1), P.Vpwrdgh(2), P.Vpwrdgh(3), "V");
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 "Power good falling threshold", P.Vpwrdgl(1), P.Vpwrdgl(2), P.Vpwrdgl(3), "V");

  printf("\n");

  ##
  ## Compensation design
  ##
  printf("=== IR3821 compensation design ===\n\n");
  if !isfield(C, "R1")
    warning("R1 not specified, compensation skipped\n")
  else
    if !isfield(C, "R2") | !isfield(C, "R3") | !isfield(C, "C1") | \
          !isfield(C, "C2") | !isfield(C, "C3")
      dotest = 0;
    else
      dotest = 1;
    endif

    design.R1 = C.R1(2);

    design.Vin  = P.vin(3);     # Design stability for maximum voltage voltage
    design.Lout = PS.Lo.Lk(2);
    design.DCR  = PS.Lo.dcr(2);
    Ceq         = 1./sum(1./(PS.Co.esr + 1./(2*pi*P.DBW*PS.Co.C*i)), 1); # effective at DBW
    design.Cout = -1./(2*pi*P.DBW*imag(Ceq(2)));
    design.ESR  = real(Ceq(2));
    design.Fsw  = P.fsw(2);
    design.G    = 1.0;          # Remote sense amplifier gain. If ever a
                                # voltage dividor is used, this must be changed.
    design.Fmax = 1e6;
    design.Fmin = 10;
    
    control.Vosc = 1.25;        # From IR3821 datasheet,
    control.GBW  = 40e6;        # No spec !
    control.DCG  = 96;		# No spec !
    control.Gm   = 1.3e-3;

    if !isfield(P, "DBW")
      design.DBW = P.fsw(1)/5;
    else
      design.DBW = P.DBW;
      if design.DBW > P.fsw(1)/3
        error("Converter bandwidth must be lower than Fsw/3")
      endif
    endif
    design.K     = P.K;

    if P.Type == 3
      if dotest == 0
        [sys, comp] = do_comp(design, control, "type3");
      else
        comp.R1 = design.R1;
        comp.R2 = C.R2(2);
        comp.R3 = C.R3(2);
        comp.C1 = C.C1(2);
        comp.C2 = C.C2(2);
        comp.C3 = C.C3(2);
        [sys, comp] = do_comp(design, control, "type3", comp);
      endif
      # Special criteria to meet since this is a transconductance
      # amplifier
      if (comp.R3 < 1/control.Gm) || (1/(1/comp.R1 + 1/comp.R2 + 1/comp.R3) < 1/control.Gm)
        error("Increase R1")
      endif
      print_comp("type3", comp);
      plot_comp(sys, 1, name);
    else
      error("To do")
    endif
    printf("\n")
  endif
endfunction
