## This file provides capacitor definitions
## 
## TODO: Correct nominal capacitance based on nominal working voltage
## and temperature.
## 
1;

function C = Capacitors_get(varargin)
  ## Usage: Capacitor_get(Cap1, N1, Cap2, N2, ...)
  len = length(varargin);
  if mod(len,2)
    error("check arguments")
  endif
  j = 1;
  for i = [1:2:len]
    C.C(j,:)   = varargin{i}.C*varargin{i+1};
    C.esr(j,:) = varargin{i}.esr/varargin{i+1};
    C.esl(j,:) = varargin{i}.esl/varargin{i+1};
    C.df(j,:)  = varargin{i}.df*varargin{i+1}; # To verify
    j = j+1;
  endfor
endfunction

function Z = Capacitors_Z(C, freq)
  ## Usage: Capacitors_Z(C, freq)
  ## Return the capacitors equivalent impedance at each frequencies

  ## Make sure freq is a colum vector
  if !isvector(freq)
    error("freq must be a vector")
  endif
  len = length(freq);
  if columns(freq) == 1
    f = freq;
  else
    f = freq';
  endif
  ## Init a conductance matrix
  Y = zeros(len, 3);

  for j = [1:size(C.C, 1)]
    Y = Y + 1./(ones(len,1)*C.esr(j,:) + 1./(2*pi*f*C.C(j,:)*i) + 2*pi*f*C.esl(j,:)*i);
  endfor

  Z = 1./Y;
endfunction

## 470uF Electrolytic 16V
function C = C_470u_16V_EL_12mm(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 470e-6*C_TOL1;
  C.esr = 0.030*ESR_TOL;
  C.esl = 10e-9*ESL_TOL;
  C.df  = 0.10;
endfunction

## 470uF POSCAP 7343 2V
function C = C_470u_2V_POSCAP_7343(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 470e-6*C_TOL1;
  C.esr = 0.006*ESR_TOL;
  C.esl = 2.2e-9*ESL_TOL;
  C.df  = 0.06;
endfunction

## 560uF POSCAP 7343 2V
function C = C_560u_2V_POSCAP_7343(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 560e-6*C_TOL1;
  C.esr = 0.003*ESR_TOL;
  C.esl = 2.2e-9*ESL_TOL;
  C.df  = 0.06;
endfunction

## 330uF ALP 7343 2V
function C = C_330u_2V_ALP_7343(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 330e-6*C_TOL1;
  C.esr = 0.006*ESR_TOL;
  C.esl = 2.2e-9*ESL_TOL;
  C.df  = 0.06;
endfunction

## 220uF Electrolytic 100V
function C = C_220u_100V_EL_12mm(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 220e-6*C_TOL1;
  C.esr = 0.200*ESR_TOL;
  C.esl = 10e-9*ESL_TOL;
  C.df  = 0.10;
endfunction

## 220uF ALP 6.3V
function C = C_220u_6V3V_ALP_7343(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 220e-6*C_TOL1;
  C.esr = 0.018*ESR_TOL;
  C.esl = 2.2e-9*ESL_TOL;
  C.df  = 0.06;
endfunction

## 150uF POSCAP CAN10mm 20V
function C = C_150u_20V_POSCAP_10mm(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 150e-6*C_TOL1;
  C.esr = 0.020*ESR_TOL;
  C.esl = 10e-9*ESL_TOL;
  C.df  = 0.10;
endfunction

## 150uF POSCAP 7343 16
function C = C_150u_16V_POSCAP_7343(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 150e-6*C_TOL1;
  C.esr = 0.060*ESR_TOL;
  C.esl = 2.2e-9*ESL_TOL;
  C.df  = 0.10;
endfunction

## 150uF POSCAP 7343 10V
function C = C_150u_10V_POSCAP_7343(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 150e-6*C_TOL1;
  C.esr = 0.012*ESR_TOL;
  C.esl = 2.2e-9*ESL_TOL;
  C.df  = 0.10;
endfunction

## 100uF Ceramic X5R 1210 6.3V
function C = C_100u_6V3_X5R_1210(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 100e-6*C_TOL1*0.8; # At 1.3V
  C.esr = 1.5-3*ESR_TOL;
  C.esl = 0.8e-9*ESL_TOL;
  C.df  = 0;      # TBD
endfunction

## 22uF Ceramic X5R 0805 6.3V
function C = C_22u_16V_X5R_1210(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 22e-6*C_TOL1*0.8; # At 5V
  C.esr = 3.3e-3*ESR_TOL;
  C.esl = 1e-9*ESL_TOL;
  C.df  = 0;      # TBD
endfunction

## 22uF Ceramic X5R 0805 6.3V
function C = C_22u_6V3_X5R_0805(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 22e-6*C_TOL1*0.8; # At 1.3V
  C.esr = 2.2e-3*ESR_TOL;
  C.esl = 0.8e-9*ESL_TOL;
  C.df  = 0;      # TBD
endfunction

## 47uF Ceramic X6S 0805 4V
function C = C_47u_4V_X6S_0805(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 47e-6*C_TOL1*0.8; # At 1.3V
  C.esr = 1.0-3*ESR_TOL;
  C.esl = 0.48e-9*ESL_TOL;
  C.df  = 0;      # TBD
endfunction


## 10uF Ceramic X5R 1206 25V
function C = C_10u_25V_X5R_1206(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 10e-6*C_TOL1*0.6; # At 12V
  C.esr = 4.6e-3*ESR_TOL;
  C.esl = 1.2e-9*ESL_TOL;
  C.df  = 0;      # TBD
endfunction

## 10uF Ceramic X5R 0603 6.3V
function C = C_10u_6V3_X5R_0603(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 10e-6*C_TOL1*0.8; # At 1.3V
  C.esr = 3.6e-3*ESR_TOL;
  C.esl = 0.8e-9*ESL_TOL;
  C.df  = 0;      # TBD
endfunction


## 2.2uF Ceramic X7R 1210 100V
function C = C_2u2_100V_X7R_1210(V, T)
  C_TOL1  = [0.7, 1, 1.3];        # 20% nominal, 10% endurance
  ESR_TOL = [0.8, 1, 1.2];        # arbitrary
  ESL_TOL = [0.9, 1, 1.1];        # arbitrary

  C.C   = 0.7e-6*C_TOL1*0.8; # At 50V
  C.esr = 8e-3*ESR_TOL;
  C.esl = 0.8e-9*ESL_TOL;
  C.df  = 0;      # TBD
endfunction
