import numpy as np


class Prm:
    def __init__(self):
        self.vref = np.array([0.594, 0.6, 0.606])
        self.Ton_min = 50e-9
        self.Toff_min = 250e-9
        self.vrefldo = np.array([0.728, 0.75, 0.772])
        self.enlr = np.array([2.4, 2.6, 2.95])
        self.enlf = np.array([2.23, 2.4, 2.57])
        self.ilim_cs = 10e-6 * np.array([0.999, 1, 1.001])  # Tolerances guessed
        self.ilim_ofs = [-10e-3, 0, 10e-3] * 0
        self.rdson = np.array([5.9, 6.8, 8.0]) * 1e-3 * 0.65  # Guest from spec: Rilim = 6.81k : Ilim = 8.5, 10, 11.5A
        self.Ton_min = 80e-9
        self.Toff_min = 250e-9
        self.dV = np.array([10, 15, 20]) * 1e-3  # Required ripple at FB pin, spec says min. 10mV
        self.iss = 3e-6 * np.array([0.90, 1, 1.10])  # tolerance guessed
        self.Vss = 1.5 * np.array([0.95, 1, 1.05])  # Tolerance guessed
        self.ilim_tol = np.array([0.85, 1, 1.15])  # Tolerance guessed


Prm = Prm()
Rtol = np.array([0.99, 1, 1.01])
Ctol = np.array([0.95, 1, 1.05])

def SC402B(P, C, PS):
    if P['phase'] != 1:
        print("Number of phase must be one")
        return -1

    print("=== SC402B component selection ===\n\n")

    print("Selected components: \n")

    if 'R1' in C and 'R2' not in C:
        if 'R3' in C:
            R1eq = 1.0 / (1.0 / C['R1'][1] + 1.0 / C['R3'][1])
        else:
            R1eq = C['R1'][1]
        C['R2'] = Prm.vref[1] * R1eq / (Prm.vref[1])
    elif 'R1' not in C and 'R2' in C:
        R1eq = (P['vout'] - Prm.vref[1]) * C['R2'][1] / (Prm.vref[1])
        if 'R3' in C:
            C['R1'] = R1eq * C['R3'][1] / (C['R3'][1] - R1eq) * Rtol
        else:
            C['R1'] = R1eq * Rtol
    elif 'R1' not in C and 'R2' not in C:
        print("R1 or R2 must be provided")
        return -1

    if 'R3' in C:
        C['R1'] = C['R1'] * C['R3'] / (C['R1'] + C['R3'])

    print("{:45s} : {:6.2f} {:6s}\n".format(" Voltage setting resistor (R1)", C['R1'][1] / 1e3, "kOhms"))
    print("{:45s} : {:6.2f} {:6s}\n".format(" Voltage setting resistor (R2)", C['R2'][1] / 1e3, "kOhms"))

    if not P.get('Rton'):
        Ton = P['vout'] / (P['vin'][1] * P['fsw'])
        C['Rton'] = P['k'] / (25e-12 * P['fsw']) * Rtol

    print("{:45s} : {:6.2f} {:6s}\n".format(" Voltage setting resistor (R2)", C['Rton'][1] / 1e3, "kOhms"))

    if C['Rton'].any() > (P['vin'][0] / 15e-6):
        print(" --> Rton too large")

    Ton = (C['Rton'] * 25e-12 * P['vout'] / (np.flip(P['vin']) * P['k'])) * [0.9, 1, 1.1]
    P['fsw'] = P['vout'] / (np.flip(Ton) * P['vin'])

    ## Minimum ripple at FB pin
    Zeq = 1.0 / sum(1.0 / (PS['Co'].esr + 1.0 / (2.0 * np.pi * P['fsw'][1] * PS['Co'].C*1j)))  # effective at Fsw
    ESR = np.real(Zeq)
    Co = -1.0 / (2.0 * np.pi * P['fsw'] * np.imag(Zeq))
    dIL = np.flip(Ton) * (P['vin'] - P['vout']) / np.flip(PS['Lo'].Lk)
    dVo = dIL * abs(Zeq)
    if (dVo*(C['R2']/(C['R1']+C['R2']))).any() < Prm.dV.any():
        if dVo.any() >= Prm.dV.any():
            if not C.get('C1'):
                print(" Capacitive feed forward compensation needed: \n")
                C['C1'] = Ton[2]*((Prm.vref[1]+0.5*Prm.dV[1])*(C['R1'][1]+C['R2'][1])-P['vout']*C['R2'][1]) / ((dVo[1]-Prm.dV[1])*C['R1'][2]*C['R2'][2])*Ctol
        else:
            if not C.get('R3') or not C.get('C1'):
                if not P.get('k'):
                    P['k'] = 3

                print(" Additionnal compensation ramp required:\n")
                Tau_off = P['k']
                Tau_on = Tau_off*(P['vout']/(P['vin'][1]-P['vout']))
                k = (1-np.exp(-Tau_on))
                #C['R3'] = (P)





