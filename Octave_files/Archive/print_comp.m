function print_comp(ctype, comp)
  ## ouput result
  if ctype == "type2"
    printf("Type 2 compensation value:\n");
    printf("  R1: %e\n", comp.R1);
    printf("  R2: %e\n", comp.R2);
    printf("  C1: %e\n", comp.C1);
    printf("  C2: %e\n", comp.C2);
  elseif ctype == "type3"
    printf("Type 3 compensation value:\n");
    printf("  R1: %e\n", comp.R1);
    printf("  R2: %e\n", comp.R2);
    printf("  R3: %e\n", comp.R3);
    printf("  C1: %e\n", comp.C1);
    printf("  C2: %e\n", comp.C2);
    printf("  C3: %e\n", comp.C3);
  else
    printf("Invalid type")
  endif
endfunction
