# Compute components from targets or compute
# achieved parameters versus targets using supplied components
# values.
#
# P   : Target parameters
# C   : Control components selection
# PS  : Power stage components selection
# name: name for plot
# sel : "vr0" or "vr1"
#
# TODO:
#       -Implement compensation design with worst case and plot
#        typ, min, max (monte carlo ?)
#       -Consider DAC accuracy variation for Vout computation
#       -Over current still not exact, need to correct effect of
#        multiphase ripple
#
function [P, C] = ISL6364(P, C, PS, name, sel)
  # Chip parameters
  Prm.ref_tol  = [0.995, 1, 1.005];
  Prm.en_pwr_r = [0.875, 0.897, 0.920];
  Prm.en_pwr_f = [0.735, 0.752, 0.770];
  if sel == "vr0"
    Prm.iocppk   = [120, 125, 135]*1e-6;
    Prm.iocavg   = [96,  100, 108]*1e-6;
  elseif sel == "vr1"
    Prm.iocavg   = [96,  100, 108]*1e-6;
  else
    error("Valid selection is vr0 or vr1")
  endif
  Prm.imon_oc  = [1.085, 1.11, 1.14];
  Prm.freq_tol = [0.9, 1.0, 1.1];

  Ctol = [0.95, 1, 1.05];
  Rtol = [0.99, 1, 1.01];

  ## 
  ## Components selection
  ## 
  printf("=== ISL6364 component selection ===\n\n");

  if P.phase > 4 || P.phase < 1
    error("Number of phase must be from 1 to 4")
  endif

  printf("Selected components:\n")

  ## DAC setting is per user
  if !isfield(P, "dac")
    error("DAC setting needs to be provided")
  endif
  printf("%-45s : %6.2f %6s\n", " Selected DAC setting", P.dac, "Volt");

  ## Switching freq
  if !isfield(C, "Rfs")
    C.Rfs       = 5e10/P.fsw*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Frequency setting resistor (Rfs)", C.Rfs(2)/1000, "kOhms");

  ## Sawtooth amplitude
  if sel == "vr0"
    if !isfield(C, "Rramp")
      Vin_mean = (P.vin(3)+P.vin(1))/2;
      C.Rramp  = 0.5*Vin_mean/12*2.4e6/1.5*Rtol;
    endif
    P.Vosc = 0.5*P.vin/12*2.4e6./C.Rramp;
    printf("%-45s : %6.2f %6s\n", " Ramp adjust resistor (Rramp)", C.Rramp(2)/1e6, "MOhms");
    if P.Vosc(1) < 0.3
      printf(" --> Minimum ramp amplitude too low")
    endif
    if P.Vosc(3) > 3.0
      printf(" --> Maximum ramp amplitude too high")
    endif
  endif

  ## DCR sensing
  if !isfield(C, "Csen")
    error("Csen must be specified")
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense capacitor (Csen)", C.Csen(2)/1e-9, "nF");
  if !isfield(C, "Rsen")
    # Valid for uncoupled inductor only
    C.Rsen      = (PS.Lo.Lk(2)/PS.Lo.dcr(2))/C.Csen(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current sense resistor (Rsen)", C.Rsen(2)/1e3, "kOhms");
  if C.Rsen > 5e3
    printf(" --> Rsen too large\n")
  endif

  ## Risen or Rset  
  if sel == "vr0"
    if !isfield(C, "Rset")
      C.Rset = P.ioc/P.phase*PS.Lo.dcr(2)/Prm.iocavg(2)*64*Rtol;
    endif
    C.Risen = C.Rset/64;      # Equivalent for later computation    
    printf("%-45s : %6.2f %6s\n", " Current sense set resistor (Rset)", C.Rset(2)/1e3, "kOhms");
    if C.Rset < 3.84e3 | C.Rset > 115.2e3
      printf(" --> Rset not in recommanded range: 3.84K to 115.2K\n")
    endif
  elseif sel == "vr1"
    if !isfield(C, "Risen")
      C.Risen     = P.ioc/P.phase*PS.Lo.dcr(2)/Prm.iocavg(2)*Rtol;
    endif
    printf("%-45s : %6.2f %6s\n", " Current sense filter resistor (Risen)", C.Risen(2)/1e3, "kOhms");
    if !isfield(C, "Ct")
      C.Ct          = 27e-9/C.Risen(2)*Ctol;
    endif
    printf("%-45s : %6.2f %6s\n", " Current sense filter capacitor (Ct)", C.Ct(2)/1e-9, "nF");
  else
    error("Specify vr0 or vr1")
  endif

  ## Load line
  if !isfield(C, "Rfb")
    C.Rfb       = P.load_line*P.phase*C.Risen(2)/PS.Lo.dcr(2)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Feedback resistor (Rfb)", C.Rfb(2)/1e3, "kOhms");

  ## Enable / disable
  printf("%-45s : %6.2f %6s\n", " Enable pull-up", C.Ruvlo_pu(2)/1e3, "kOhms");
  if !isfield(C, "Ruvlo_pd")
    C.Ruvlo_pd   = Prm.en_pwr_r(2)*C.Ruvlo_pu(2)/(P.uvlo_r-Prm.en_pwr_r(2))*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Enable pull-down", C.Ruvlo_pd(2)/1e3, "kOhms");

  ## Imon
  if !isfield(C, "Rimon")
    C.Rimon   = Prm.imon_oc(2)*P.phase*C.Risen(2)/(PS.Lo.dcr(2)*P.ioc)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Current monitoring scaling resistor (Rimon)", C.Rimon(2)/1e3, "kOhms");

  printf("\n");

  ##
  ## Printout actual parameters
  ##
  printf("Achieved parameters:\n")
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");

  ## Switching freq
  P.fsw = 5e10*Prm.freq_tol./fliplr(C.Rfs);
  fsw = P.fsw/1000;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");

  ## Ramp amplitude
  if sel == "vr0"
    printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	   " Ramp amplitude", P.Vosc(1), P.Vosc(2), P.Vosc(3), "V");
  endif

  ## Nominal output voltage
  P.vout = P.dac.*Prm.ref_tol;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Output voltage", P.vout(1), P.vout(2), P.vout(3), "V");

  ## Duty cycle estimation
  P.duty = P.vout./fliplr(P.vin);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
         " Duty cycle (ideal)", P.duty(1)*100, P.duty(2)*100, P.duty(3)*100, "%");

  ## DCR sensing matching
  m = ((PS.Lo.Lk./fliplr(PS.Lo.dcr))./fliplr(C.Rsen.*C.Csen) - 1)*100;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " DCR time constant matching", m(1), m(2), m(3), "%");

  ## Load line
  P.load_line = C.Rfb.*PS.Lo.dcr./fliplr(P.phase*C.Risen);
  Rll = P.load_line*1000;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Load line", Rll(1), Rll(2), Rll(3), "mOhm");

  ## Overcurrent setting: compute real inductor current corresponding
  ## to sensed current, neglecting saturation. Consider coupling and
  ## multiphase effect.
  dIL    = P.vout.*(1-P.duty)./fliplr(PS.Lo.Lk.*P.fsw);
  if sel == "vr0"
    #P.iocppk = C.Risen.*Prm.iocppk.*C.Csen.*C.Rsen./fliplr(PS.Lo.Lk);
    #printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
    #	   " Overcurrent (peak per phase)", P.iocppk(1), P.iocppk(2),
    #	   P.iocppk(3), "A");
    ## Phase average current
    P.iocp_avg = (C.Risen.*Prm.iocppk - fliplr(dIL/2).*(PS.Lo.Lk)./(C.Rsen.*C.Csen))./fliplr(PS.Lo.dcr);
    ## Corresponding peak current  
    if PS.Lo.Cpl == 1
      P.iocp_pk  = P.iocp_avg + dIL/2;
    elseif PS.Lo.Cpl == 2
      P.iocp_pk  = P.iocp_avg + dIL/4;
    else
      error(" --> Coupling value unsupported\n");
    endif
      
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	   " Overcurrent (per phase, avg)", P.iocp_avg(1), P.iocp_avg(2), P.iocp_avg(3), "A");
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	   " Overcurrent (per phase, peak)", P.iocp_pk(1), P.iocp_pk(2), P.iocp_pk(3), "A");
  endif
  ## All phase average current
  P.ioc_avg = (C.Risen.*Prm.iocavg - fliplr(dIL/2).*(PS.Lo.Lk)./(P.phase*C.Rsen.*C.Csen))./fliplr(PS.Lo.dcr)*P.phase;
  P.ioc_pk  = P.ioc_avg + dIL/2./P.phase;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (total, avg)", P.ioc_avg(1), P.ioc_avg(2), P.ioc_avg(3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (total, peak)", P.ioc_pk(1), P.ioc_pk(2), P.ioc_pk(3), "A");
  #P.ioc = C.Risen.*Prm.iocavg.*C.Csen.*C.Rsen./fliplr(PS.Lo.Lk)*P.phase;
  #printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  #	 " Overcurrent (peak phase avg)", P.ioc(1), P.ioc(2), P.ioc(3), "A");

  ## Isen+ time constant matching
  if sel == "vr1"
    m = (C.Ct.*C.Risen/27e-9 - 1)*100;
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   " Isen+ time constant matching", m(1), m(2), m(3), "%");
  endif

  ## Enable / disable voltage
  uvlo_r = Prm.en_pwr_r.*(C.Ruvlo_pu+fliplr(C.Ruvlo_pd))./fliplr(C.Ruvlo_pd);
  uvlo_f = Prm.en_pwr_f.*(C.Ruvlo_pu+fliplr(C.Ruvlo_pd))./fliplr(C.Ruvlo_pd);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Enable rising threshold", uvlo_r(1), uvlo_r(2), uvlo_r(3), "V");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Enable falling threshold", uvlo_f(1), uvlo_f(2), uvlo_f(3), "V");

  ## Imon
  ioc = Prm.imon_oc*P.phase.*C.Risen./fliplr(PS.Lo.dcr.*C.Rimon);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " IMON over current", ioc(1), ioc(2), ioc(3), "A");
  gain = P.phase.*C.Risen./fliplr(PS.Lo.dcr.*C.Rimon);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " IMON gain", gain(1), gain(2), gain(3), "A/V");

  printf("\n");

  ## 
  ## Compensation design
  ## 
  if !isfield(C, "R1")
    warning("R1 must be specified, compensation skipped")
  else
    if !isfield(C, "R2") | !isfield(C, "R3") | !isfield(C, "C1") | \
            !isfield(C, "C2") | !isfield(C, "C3")
      dotest = 0;
    else
      dotest = 1;
    endif

    design.R1 = C.R1(2);

    design.Vin  = P.vin(3);       # Design stability for maximum voltage voltage

    # Inductor value handled considering coupled inductor
    if PS.Lo.Cpl == 2
      if P.phase == 1
        design.Lout = PS.Lo.Lm(2);
        design.DCR  = PS.Lo.dcr(2);
      elseif mod(P.phase, PS.Lo.Cpl)
        error("Phase number must be consistant with number of coupled phases!\n")
      else
        design.Lout = PS.Lo.Lk(2)/P.phase;
        design.DCR  = PS.Lo.dcr(2)/P.phase;
      endif
    elseif PS.Lo.Cpl == 1
      design.Lout = PS.Lo.Lk(2)/P.phase;
      design.DCR  = PS.Lo.dcr(2)/P.phase;
    else
      error("More than two phases coupled inductor not supported\n")
    endif

    Ceq         = 1./sum(1./(PS.Co.esr + 1./(2*pi*P.DBW*PS.Co.C*i)), 1); # effective at DBW
    design.Cout = -1./(2*pi*P.DBW*imag(Ceq(2)));
    design.ESR  = real(Ceq(2));
    design.Fsw  = P.fsw(2);
    if isfield(P, "G")
      design.G = P.G;
    else
      design.G    = 1.0;        # Remote sense amplifier gain. If ever a
                                # voltage dividor is used, this must be
                                # changed.
    endif
    if isfield(P, "K")
      design.K    = P.K;
    else
      design.K = 1;
    endif
    design.Fmax = 1e6;
    design.Fmin = 10;

    if sel == "vr0"
      control.Vosc = P.Vosc(2);
    else
      control.Vosc = 2.0;           # From ISL6364 datasheet,
    endif
    control.GBW  = 40e6;		# error amp gain bandwidth
    control.DCG  = 96;		# error amp DC gain in dB
    if !isfield(P, "DBW")
      design.DBW = P.fsw(1)/5;
    else
      design.DBW = P.DBW;
      if design.DBW > P.fsw(1)/3
        error("Converter bandwidth must be lower than Fsw/3")
      endif
    endif

    if P.Type == 3
      t = "type3"
    else
      if P.Type == 2
        t = "type2"
      else
        error("Type not supported")
      endif
    endif

    if dotest == 0
      [sys, comp] = do_comp(design, control, t);
    else
      comp.R1 = design.R1;
      comp.R2 = C.R2;
      comp.R3 = C.R3;
      comp.C1 = C.C1;
      comp.C2 = C.C2;
      comp.C3 = C.C3;
      [sys, comp] = do_comp(design, control, t, comp);
    endif
    print_comp(t, comp);
    plot_comp(sys, 1, name);

    printf("\n");

  endif
endfunction
