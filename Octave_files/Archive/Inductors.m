## This file provides inductors definitions
## 
## TODO: Correct inductance based on maximum expected current and temperature
## 
1;

function L = Inductors_get(varargin)
  ## Usage: Inductors_get(Ind1, N1, Ind2, N2, ...)
  len = length(varargin);
  if mod(len,2)
    error("check arguments")
  endif
  j = 1;
  for i = [1:2:len]
    L.Cpl(j) = varargin{i}.Cpl;
    L.Lk(j,:)  = varargin{i}.Lk/varargin{i+1};
    if L.Cpl(j) == 1
      L.L = L.Lk;
    endif
    L.Lm(j,:)  = varargin{i}.Lm/varargin{i+1};
    L.dcr(j,:) = varargin{i}.dcr/varargin{i+1};
    j = j+1;
  endfor
endfunction

## 
## Single inductor
## 

## 10uH Vishal IHLP5050
function L = L_10u_10A_16mR_5050(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 10e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 16.4e-3*[0.925, 1, 1.075];
endfunction

## 4.7uH Vishay IHCL2525
function L = L_4u7_5A_37mR_2525(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 4.7e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 37e-3*[0.925, 1, 1.075];
endfunction

## 3.3uH Vishay IHCL2525
function L = L_3u3_6A_28mR_2525(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 3.3e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 28e-3*[0.925, 1, 1.075];
endfunction

## 2.2uH Vishay IHCL2525
function L = L_2u2_8A_18mR_2525(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 2.2e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 18e-3*[0.925, 1, 1.075];
endfunction

## 1.5uH Vishay IHCL4040
function L = L_1u5_15A_5mR_4040(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 1.5e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 5.3e-3*[0.925, 1, 1.075];
endfunction

## 1.5uH Vishay IHCL2525
function L = L_1u5_9A_14mR_2525(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 1.5e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 14e-3*[0.925, 1, 1.075];
endfunction

## 1.0uH Vishay IHCL4040
function L = L_1u0_17A_4mR_4040(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 1.0e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 3.7e-3*[0.925, 1, 1.075];
endfunction

## 1.0uH Vishay IHCL2525
function L = L_1u0_11A_9mR_2525(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 1.0e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 9e-3*[0.925, 1, 1.075];
endfunction

## 1.0uH Vishay IHCL2525
function L = L_1u0_4A_36mR_1616(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 1.0e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = 36e-3*[0.925, 1, 1.075];
endfunction

## 680nH Vishay IHCL2525
function L = L_680n_10A_9mR_2020(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 680e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 9e-3*[0.95, 1, 1.05];
endfunction

## 1.0uH Vishay IHCL4040
function L = L_560n_27A_1m7R_4040(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 0.56e-6*L_TOL1;
  L.Lm  = 0;
  L.dcr = [1.6, 1.7, 1.8]*1e-3;
endfunction

## 470nH Vishay IHCL2525
function L = L_470n_17A_4mR_2525(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 470e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 4e-3*[0.95, 1, 1.05];
endfunction

## 470nH Vishay IHCL2525
function L = L_220n_23A_3mR_2525(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation
  
  L.Cpl = 1;
  L.Lk  = 220e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 2.5e-3*[0.95, 1, 1.05];
endfunction

## 230nH power ferrite inductor
function L = L_230n_37A_290uR_11x8mm(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation

  L.Cpl = 1;
  L.Lk  = 230e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 0.29e-3*[0.92, 1, 1.08];
endfunction

## 220nH power inductor 1043-7718
function L = L_220n_35A_900uR_11x11mm(Imax, T)
  L_TOL1  = [0.56 1, 1.2];        # 20% nominal, -30% for saturation

  L.Cpl = 1;
  L.Lk  = 220e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 0.9e-3*[0.9, 1, 1.1];
endfunction

function L = L_220n_35A_800uR_10x10mm(Imax, T)
  L_TOL1  = [0.56 1, 1.2];        # 20% nominal, -30% for saturation

  L.Cpl = 1;
  L.Lk  = 220e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 0.8e-3*[0.8, 1, 1.2];
endfunction

## 155nH power ferrite inductor
function L = L_155n_31A_390uR_10x7mm(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation

  L.Cpl = 1;
  L.Lk  = 155e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 0.37e-3*[0.92, 1, 1.08];
endfunction

## 80nH ferrite bead
function L = L_80n_6A_4mR_1206(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation

  L.Cpl = 1;
  L.Lk  = 80e-9*L_TOL1;
  L.Lm  = 0;
  L.dcr = 4e-3*[0.8, 1, 1.2];
endfunction

## 
## Coupled inductor
## 

## 90nH/300nH couple power ferrite inductor
function L = LC_90n_60A_440uR_14x10mm(Imax, T)
  L_TOL1  = [0.64, 1, 1.2];        # 20% nominal, -20% for saturation

  L.Cpl = 2;
  L.Lk  = 90e-9*L_TOL1;
  L.Lm  = 300e-9*L_TOL1;
  L.dcr = 0.44e-3*[0.9, 1, 1.1];
endfunction
