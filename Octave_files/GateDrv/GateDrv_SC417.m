function [Tg, Bg] = GateDrv_SC417(Pvcc, t)
  Tg.Vdr = Pvcc;
  Tg.Rpu = [1.25, 2.00, 3.00];
  Tg.Rpd = [0.90, 1.65, 3.00];

  Bg.Vdr = Pvcc;
  Bg.Rpu = [0.85, 1.25, 2.20];
  Bg.Rpd = [0.60, 0.80, 1.35];
  Bg.Dt  = 10e-9;
endfunction