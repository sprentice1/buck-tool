# Compute power stage characteristics using
# operating parameters P and power components C
#
# TODO: - fixe power losses estimate
#       - many parameter are rough approximation and
#         should be fixed for coupled inductor case
#       - This is work in progress
#
# Compute DC-DC power stage power losses, RMS current ans so on
# for minimum, typical and maximum parameters.
# 
# Currently support only uncoupled and 2x2 coupled inductors.
#
# Assumption / limitation:
#  - Current and voltage ripple are small and neglected in operating
#    point computation.
#  - Only resistive losses considered for operating point computation.
#  - Output ripple is an approximation, considering the ripple as a full
#    amplitude sine
#
# Required input data:
#  P = { }
#  C = { }
#
#
# TODO: debug small inconsistancy in computed power. See R.Perr.
function R = Pstage(P, C)

  # compute duty cycle considering power losses but
  # neglecting the ripple current and ripple voltage effect
  for i = [1:length(P.iout)]
    R.iout(i, :) = P.iout(i);
    # Vout with droop and offset
    R.vout(i, :)   = P.vout - fliplr(P.load_line)*P.iout(i);
    R.Pout(i, :)   = R.vout(i, :).*P.iout(i);
    # Duty cycle
    Iph         = P.iout(i)/P.phase;
    R.Iphase(i, :) = Iph;
    R.duty(i, 1)   = min(roots([-Iph*C.Li.dcr(1)*P.phase,
				(P.vin(3)-Iph*C.Tf.Rds(1)+Iph*C.Bf.Rds(1)),
				-Iph*C.Bf.Rds(1)-R.vout(i, 1)-Iph*C.Lo.dcr(1)]));
    R.duty(i, 2)   = min(roots([-Iph*C.Li.dcr(2)*P.phase,
				(P.vin(2)-Iph*C.Tf.Rds(2)+Iph*C.Bf.Rds(2)),
				-Iph*C.Bf.Rds(2)-R.vout(i, 2)-Iph*C.Lo.dcr(2)]));
    R.duty(i, 3)   = min(roots([-Iph*C.Li.dcr(3)*P.phase,
				(P.vin(1)-Iph*C.Tf.Rds(3)+Iph*C.Bf.Rds(3)),
				-Iph*C.Bf.Rds(3)-R.vout(i, 3)-Iph*C.Lo.dcr(3)]));
    # Vbulk voltage
    Vblk = P.vin - C.Li.dcr.*P.iout(i).*R.duty(i, :);

    # Phase ripple computation depends on inductor type
    if C.Lo.Cpl == 1
      ## Phase ripple
      R.Iphase_ripple(i, :) = R.vout(i, :).*(1-R.duty(i, :))./fliplr(C.Lo.Lk.*P.fsw);
      ## Peak phase current
      R.Iphase_peak(i, :)   = R.Iphase(i, :)+R.Iphase_ripple(i, :)/2;
      ## Output ripple
      N  = P.phase;
      D  = R.duty(i, :);
      m  = floor(N*D);
      Ki = (N*(D-m/N).*((m+1)/N - D)) ./ (D.*(1-D));
      R.Iout_ripple(i, :) = R.Iphase_ripple(i, :).*Ki;
    elseif C.Lo.Cpl == 2
      if mod(P.phase, C.Lo.Cpl)
        error("Phase number must be consistant with number of coupled \
            phases!\n")
        return
      endif
      ## Phase ripple
      p = C.Lo.Lm./fliplr(C.Lo.Lk);
      R.Iphase_ripple1(i, :) = Vblk./fliplr(C.Lo.Lk).*((1+p)./(1+2*p)-R.duty(i, :)).*R.duty(i, :).*1./fliplr(P.fsw);
      R.Iphase_ripple2(i, :) = Vblk./fliplr(C.Lo.Lk).*((p)./(1+2*p)-R.duty(i, :)).*R.duty(i, :).*1./fliplr(P.fsw);
      ## Total phase ripple for one coupled inductor
      R.Iphase_ripplet(i, :) = Vblk./fliplr(C.Lo.Lk).*(1-2*R.duty(i, :)).*R.duty(i, :).*1./fliplr(P.fsw);
      ## Mean phase ripple current
      R.Iphase_ripple(i, :)  = R.Iphase_ripple1(i, :);
      ## Peak phase current
      R.Iphase_peak(i, :)    = R.Iphase(i, :)+R.Iphase_ripple1(i, :)/2;
      ## Output ripple
      N  = P.phase / C.Lo.Cpl;
      D  = R.duty(i, :);
      m  = floor(N*D);
      Ki = (N*(D-m/N).*((m+1)/N - D)) ./ (D.*(1-D));
      R.Iout_ripple(i, :) = R.Iphase_ripplet(i, :).*Ki;
    else
      error("Number of coupled phase not supported\n")
      return
    endif
    # Ripple voltage at output, approximation neglecting
    # harmonics and difference Zeq at other frequencies.
    Zeq = 1./sum(1./(2*pi*ones(size(C.Co.esl, 1),1)*P.fsw*P.phase.*C.Co.esl*j
                     + C.Co.esr
                     + 1./(2*pi*fliplr(ones(size(C.Co.C, 1),1)*P.fsw*P.phase).*C.Co.C*j)),
                 1);
    R.Vo_ripple(i, :)      = R.Iout_ripple(i, :).*fliplr(abs(Zeq));
    # Input mean current
    R.Iin_mean(i, :)       = P.iout(i)*R.duty(i, :);
    # Cout RMS current
    R.ICout_rms(i, :)      = R.Iout_ripple(i, :)/(2*sqrt(3));
    # Phase RMS current
    Iphr                   = R.Iphase_ripple(i, :);
    R.Iphase_rms(i, :)     = R.Iphase(i, :)*sqrt(1+1/3*(0.5*Iphr./R.Iphase(i, :)).^2);
    # Top fet RMS current
    R.Itf_rms(i, :)        = Iph*sqrt(R.duty(i, :).*(1+1/3*(0.5*Iphr./Iph).^2));
    # Bottom fet RMS current
    R.Ibf_rms(i, :)        = Iph*sqrt(fliplr(1-R.duty(i, :)).*(1+1/3*(0.5*Iphr./Iph).^2));
    # Cin RMS current
    D                      = P.phase*R.duty(i,:);
    if (D > 1)
      error("Formula valid for input current duty cycle below 100%")
    endif
    #Icin1                  = Iph-R.Iin_mean(i, :);
    #Icin2                  = R.Iin_mean(i, :);
    #R.ICin_rms(i, :)       = sqrt(Icin1.^2.*D.*(1+1/3.*(0.5*Iphr./Icin1).^2)+Icin2.^2.*(1-D));
    I1                     = Iph - Iphr/2 - R.Iin_mean(i, :);
    I2                     = Iph + Iphr/2 - R.Iin_mean(i, :);
    R.ICin_rms(i, :)       = sqrt(D/3.*(I1.^2+I1.*I2+I2.^2) + R.Iin_mean(i, :).^2.*(1-D));
    # Resistive losses
    Ceq                    = 1./sum(1./(C.Co.esr + 1./(2*pi*P.fsw(2)*P.phase.*C.Co.C*j)), 1);
    Ceq_esr                = real(Ceq);
    R.Pcout(i, :)          = Ceq_esr.*R.ICout_rms(i, :).^2;

    R.PLo(i, :)            = C.Lo.dcr.*R.Iphase_rms(i, :).^2*P.phase;
    R.PTf(i, :)            = C.Tf.Rds.*R.Itf_rms(i, :).^2*P.phase;
    R.PBf(i, :)            = C.Bf.Rds.*R.Ibf_rms(i, :).^2*P.phase;

    Ceq                    = 1./sum(1./(C.Ci.esr + 1./(2*pi*P.fsw(2)*P.phase*C.Ci.C*j)), 1);
    Ceq_esr                = real(Ceq);
    R.Pcin(i, :)           = Ceq_esr.*R.ICin_rms(i, :).^2;

    R.PLi(i, :)            = C.Li.dcr.*R.Iin_mean(i, :).^2;
    # Switching losses top fet
    Tsw                 = C.Tf.Qsw./((C.Tfdr.Vdr-C.Tf.Vsp)./(C.Tfdr.Rpu+C.Tf.Rg)) \
	+ C.Tf.Qsw./(C.Tf.Vsp./(C.Tfdr.Rpd+C.Tf.Rg));
    R.PTfsw(i, :)       = max(P.vin.*(R.Iphase(i, :)-R.Iphase_ripple(i, :)/2).*P.fsw.*Tsw*P.phase, 0);
    R.PTfg(i, :)        = C.Tfdr.Vdr.*C.Tf.Qt.*P.fsw*P.phase;

    # Switching losses bottom fet
    Tsw1                = (C.Bf.Rg+C.Bfdr.Rpu).*C.Bf.Ciss.*(log(C.Bfdr.Vdr./(C.Bfdr.Vdr-C.Bf.Vsp))
							  - log(C.Bfdr.Vdr./(C.Bfdr.Vdr-C.Bf.Vth)));
    Tsw2                = (C.Bf.Rg+C.Bfdr.Rpu).*C.Bf.Ciss.*(log(C.Bfdr.Vdr./(C.Bfdr.Vdr*0.9))
							  - log(C.Bfdr.Vdr./(C.Bfdr.Vdr-C.Bf.Vsp)));
    Tsw3                = (C.Bf.Rg+C.Bfdr.Rpd).*C.Bf.Ciss.*(log(C.Bf.Vsp./C.Bf.Vth));
    Tsw4                = (C.Bf.Rg+C.Bfdr.Rpd).*C.Bf.Ciss.*(log(C.Bfdr.Vdr*0.9./C.Bf.Vsp));

    R.PBfsw(i, :)       = ((Tsw1+Tsw3).*C.D.Vf +
			   (Tsw2+Tsw4).*(C.D.Vf + (R.Iphase(i, :)+R.Iphase_ripple(i, :)/2)
					*1.1.*C.Bf.Rds)/2
			   ).*(R.Iphase(i, :)+R.Iphase_ripple(i, :)/2).*P.fsw*P.phase;
    R.PBfg(i, :)        = C.Bfdr.Vdr.^2.*C.Bf.Ciss.*P.fsw/2*P.phase;

    # Diode losses
    R.Pdio(i, :)        = C.Bfdr.Dt.*C.D.Vf.*(R.Iphase(i, :)+R.Iphase_ripple(i, :)/2).*P.fsw*P.phase;
    # Input power
    R.Pin(i,:)          = fliplr(P.vin).*R.Iin_mean(i,:);
  endfor

  # Total losses
  R.Pt = R.Pcout+R.PLo+R.PTf+R.PBf+R.Pcin+R.PLi+R.PTfsw+R.PTfg+R.PBfsw+R.PBfg+R.Pdio;
  R.Perr = (R.Pin-R.Pout) - (R.PLo+R.PLi+R.PTf+R.PBf);

  ##
  ## Compute RMS current into each output capacitors type
  ##
  N         = 50;                # Number of harmonics to consider
  ## TODO: verify if need to flip 1/duty
  M         = (1./(R.duty(i,:)*P.phase));
  K         = size(C.Co.C)(1);
  R.Ico_rms = zeros(K, 3);
  for n = [1:N]
    ## Output caps equivalent at current frequency
    ZCoeq   = 1./sum(1./(C.Co.esr + 1./(2*pi*n.*(ones(K,1)*fliplr(P.fsw)*P.phase).*C.Co.C*j)), 1);
    ## Ripple current at current frequency, fourrier for triangular wave
    Bn      = 2./((M-1)*pi^2).*-1*(-1).^n.*M.^2./(n.^2).*sin(n*(M-1).*pi./M);
    Irms    = R.Iout_ripple(i,:)/2.*Bn/sqrt(2);
    ## For each capacitor type, adds-up the Irms^2 it gets at that
    ## frequency
    for k=[1:K]
      # TODO: Do we need to flip 1/(2pifC)
      Zco            = C.Co.esr(k,:) + 1./(2*pi*n.*fliplr(P.fsw*P.phase).*C.Co.C(k,:)*j);
      # RMS component in capacitor
      Ico_rms        = ZCoeq ./ Zco .* Irms;
      # Adding up Irms^2
      R.Ico_rms(k,:) = R.Ico_rms(k,:) + abs(Ico_rms).^2;
    endfor
  endfor
  R.Ico_rms = sqrt(R.Ico_rms);
  ##
  ## Compute RMS current into input capacitors and filter inductor
  ##
  N = 50;
  K         = size(C.Ci.C)(1);
  R.Ici_rms = zeros(K, 3);
  I1        = R.iout(i)/P.phase - R.Iphase_ripple(i, :)/2;
  I2        = R.iout(i)/P.phase + R.Iphase_ripple(i, :)/2;
  D         = R.duty(i,:)*P.phase;
  A0        = (I1+I2)/2.*D;
  R.Ili_rms = 0;
  for n = [1:N]
    ## input impedance of capacitors and inductor in parallel
    Zcieq   = 1./sum(1./(C.Ci.esr + 1./(2*pi*n.*(ones(K,1)*fliplr(P.fsw)*P.phase).*C.Ci.C*j)), 1);
    Zieq    = 1./(1./Zcieq + 1./(C.Li.dcr + 2*pi*n*fliplr(P.fsw)*P.phase.*C.Li.L*j));
    ## Ripple current at current frequency, fourrier for input current
    An      = sin(pi*D*n).*(I2+I1)/(pi*n);
    Bn      = (sin(pi*D*n)-pi*D*n.*cos(pi*D*n)).*(I2-I1)./(pi^2*D*n^2);
    In_rms  = abs(An + Bn*j)/sqrt(2);
    for k=[1:K]
      # Capacitor impedance
      Zci     = C.Ci.esr(k,:) + 1./(2*pi*n.*fliplr(P.fsw*P.phase).*C.Ci.C(k,:)*j);
      # RMS component in capacitor
      Ici_rms = Zieq ./ Zci .* In_rms;
      # Adding up Irms^2
      R.Ici_rms(k,:) = R.Ici_rms(k,:) + abs(Ici_rms).^2;
    endfor
    # Input inductor case
    ZLi       = C.Li.dcr + 2*pi*n.*fliplr(P.fsw*P.phase).*C.Li.L*j;
    Ili_rms   = Zieq ./ ZLi .* In_rms;
    R.Ili_rms = R.Ili_rms + abs(Ili_rms).^2;
  endfor
  R.Ici_rms = sqrt(R.Ici_rms);
  R.Ili_rms = sqrt(R.Ili_rms + A0.^2);

  ## 
  ## Print usefull design data
  ## 
  printf("=== Power stage parameters ===\n");
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");
  printf("%-35s : %5s  %5s  %5.1f %-5s\n",
	 "For maximum output current", "", "", R.iout(i), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Duty cycle",
         R.duty(i, 1)*100,
         R.duty(i, 2)*100,
         R.duty(i, 3)*100, "%");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Mean input current",
         R.Iin_mean(i, 1),
         R.Iin_mean(i, 2),
         R.Iin_mean(i, 3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Phase ripple (pk-pk)",
         R.Iphase_ripple(i, 1),
         R.Iphase_ripple(i, 2),
         R.Iphase_ripple(i, 3), "A");
  if (C.Lo.Cpl == 1)
    ;
  elseif (C.Lo.Cpl == 2)
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   "Phase imbalance current (+/-)",
           R.Iphase_ripple1(i, 1)-R.Iphase_ripple2(i, 1),
           R.Iphase_ripple1(i, 2)-R.Iphase_ripple2(i, 2),
           R.Iphase_ripple1(i, 3)-R.Iphase_ripple2(i, 3), "A");
  else
    warning("number of coupled phase not supported")
  endif
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Phase current (peak)",
         R.Iphase_peak(i, 1),
         R.Iphase_peak(i, 2),
         R.Iphase_peak(i, 3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Output current ripple (pk-pk)",
         R.Iout_ripple(i, 1),
         R.Iout_ripple(i, 2),
         R.Iout_ripple(i, 3), "A");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Output voltage ripple (pk-pk)",
         R.Vo_ripple(i, 1)*1e3,
         R.Vo_ripple(i, 2)*1e3,
         R.Vo_ripple(i, 3)*1e3, "mV");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Input inductor RMS current",
         R.Ili_rms(1),
         R.Ili_rms(2),
         R.Ili_rms(3), "Arms");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Output inductor RMS current",
         R.Iphase_rms(i, 1),
         R.Iphase_rms(i, 2),
         R.Iphase_rms(i, 3), "Arms");
  if 0
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   "Input caps. RMS current",
           R.ICin_rms(i, 1),
           R.ICin_rms(i, 2),
           R.ICin_rms(i, 3), "Arms");
  else
    printf("Input caps. RMS current:\n")
  endif
  for k=[1:size(C.Ci.C)(1)]
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   sprintf("  Input caps type %d", k),
           R.Ici_rms(k,1),
           R.Ici_rms(k,2),
           R.Ici_rms(k,3), "Arms");
  endfor
  if 0
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   "Output caps. RMS current",
           R.ICout_rms(i, 1),
           R.ICout_rms(i, 2),
           R.ICout_rms(i, 3), "Arms");
  else
    printf("Output caps. RMS current:\n");
  endif
  for k=[1:size(C.Co.C)(1)]
    printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	   sprintf("  Output caps type %d", k),
           R.Ico_rms(k,1),
           R.Ico_rms(k,2),
           R.Ico_rms(k,3), "Arms");
  endfor
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Top FET RMS current",
         R.Itf_rms(i, 1),
         R.Itf_rms(i, 2),
         R.Itf_rms(i, 3), "Arms");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 "Bottom FET RMS current",
         R.Ibf_rms(i, 1),
         R.Ibf_rms(i, 2),
         R.Ibf_rms(i, 3), "Arms");
endfunction
