# Compute components from targets or compute
# achieved parameters with supplied components values
#
# P   : Target parameters
#       P.vin
#       P.vout
#       P.iout
#       P.fsw
#       P.vpeak
# C   : Control components selection
#       C.R1
#       C.R2
#       C.R3
# PS  : Power stage components selection
#       PS.Lo
#       PS.Co
#
function [P, C] = NB681A(P, C, PS, name)
  Prm.Ton_min  = 50e-9;
  Prm.Toff_min = 250e-9;
  Prm.Tss = [1.1 1.3 1.4];
  Prm.Ilim_ls = [7 7.6 8.5];
  Prm.Vout_tol = [0.99 1 1.01];

  Ctol = [0.85, 1, 1.15];
  Rtol = [0.99, 1, 1.01];

  if P.phase != 1
    error("Number of phase must be one")
  endif

  D = P.vout./P.vin;

  printf("=== NB681A component selection ===\n\n");

  printf("Selected components:\n")

  Ton = (P.vout./(P.fsw.*P.vin)).*[0.9, 1, 1.1];
  Toff = ((1-D)./P.fsw).*[0.9 1 1.1];
  P.fsw = fliplr(P.vout./(Ton.*P.vin));

  if P.vin(2) == 2.*P.vout
    Icin = fliplr(P.iout./2);
    dVin = fliplr(0.25.*(P.iout./(P.fsw.*PS.Ci.C)));
  else
    Icin = fliplr(P.iout.*sqrt((P.vout./P.vin).*(1-(P.vout./P.vin))));
    dVin = fliplr((P.iout./(P.fsw.*PS.Ci.C)).*(P.vout./P.vin).*(1-P.vout./P.vin));
  endif

  ir = Ton.*(P.vin-P.vout)./fliplr(PS.Lo.Lk);
  P.vout = P.vout.*Prm.Vout_tol;
  P.Tss = [1.8e-3, 3e-3, 4.2e-3];

  R = PS.Co.esr;
  C = PS.Co.C;

  dVout = (P.vout)./(P.fsw.*PS.Lo.Lk).*(1-P.vout./P.vin).*(PS.Co.esr + 1./(8*P.fsw.*PS.Co.C));

  Vout_min = -(ir.*R.^2.*C./Ton)-(ir./(2.*C.*Ton)).*((Ton./2).^2-(R.*C).^2);
  Vout_max = ir.*R.^2.*C./Toff + (ir./(2.*C.*Toff)).*((Toff./2).^2 - (R.*C).^2);

  Cout_max = (P.ioc-P.iout).*P.Tss./P.vout;
  Vout = Vout_max - Vout_min;

  if (Ton./2)-R.*C>0 && (Toff./2)-R.*C>0
      Vout = Vout_max - Vout_min;
  end

  if R.*C > Ton./2 && R.*C > Toff./2
      Vout = ir.*R;
  end

  if R.*C > Ton./2 && R.*C < Toff./2
      Vout = Vout_max + ir.*R./2;
  end

  if R.*C < Ton./2 && R.*C > Toff./2
      Vout = -Vout_min + ir.*R./2;
  end

  fsw   = P.fsw./1e3;

  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Ton", Ton(1)*1e9, Ton(2)*1e9, Ton(3)*1e9, "ns");

  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Toff", Toff(1)*1e9, Toff(2)*1e9, Toff(3)*1e9, "ns");

  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	" Vout (DC)", P.vout(1), P.vout(2), P.vout(3), "V");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Output ripple voltage", Vout(1)*1000, Vout(2)*1000, Vout(3)*1000, "mV");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Output ripple Current", ir(1), ir(2), ir(3), "A");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Input capacitor Current", Icin(1), Icin(2), Icin(3), "A");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Ripple input voltage", 1000*dVin(1), 1000*dVin(2), 1000*dVin(3), "mV");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Cout max for Tss", 1e6*dVin(1), 1e6*dVin(2), 1e6*dVin(3), "uF");

endfunction
