# Compute components from targets or compute
# achieved parameters with supplied components values
#
# P   : Target parameters
#       P.vin
#       P.vout
#       P.iout
#       P.fsw
#       P.vpeak
# C   : Control components selection
#       C.R1
#       C.R2
#       C.R3
# PS  : Power stage components selection
#       PS.Lo
#       PS.Co
#
function [P, C] = SC431A(P, C, PS, name)
  Prm.vref     = [0.594, 0.60, 0.606];
  Prm.Ton_min  = 50e-9;
  Prm.Toff_min = 250e-9;
  Ctol = [0.85, 1, 1.15];
  Rtol = [0.99, 1, 1.01];

  if P.phase != 1
    error("Number of phase must be one")
  endif

  D = P.vout./P.vin;

  printf("=== SC431A component selection ===\n\n");

  printf("Selected components:\n")

  ## Output voltage
  if isfield(C, "R1") && !isfield(C, "R2")
    if isfield(C, "R3")
      R1eq = 1/(1/C.R1(2)+1/C.R3(2));
    else
      R1eq = C.R1(2);
    endif
    C.R2 = Prm.vref(2)*R1eq/(P.vout-Prm.vref(2))*Rtol;
  elseif !isfield(C, "R1") && isfield(C, "R2")
    R1eq = (P.vout - Prm.vref(2))*C.R2(2)./(Prm.vref(2));
    if isfield(C, "R3")
      C.R1 = R1eq*C.R3(2)/(C.R3(2)-R1eq)*Rtol;
    else
      C.R1 = R1eq*Rtol;
    endif
  elseif !isfield(C, "R1") && !isfield(C, "R2")
    error("R1 or R2 must be provided")
  endif

  if isfield(C, "R3")
    C.R1 = C.R1.*C.R3./(C.R1+C.R3);
  endif
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (R1)", C.R1(2)/1e3, "kOhms");
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (R2)", C.R2(2)/1e3, "kOhms");

  P.vout = Prm.vref./(fliplr(C.R2)./(C.R1+fliplr(C.R2)));
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	" Vout (DC)", P.vout(1), P.vout(2), P.vout(3), "V");

  Ton = (P.vout./(P.fsw.*P.vin)).*[0.9, 1, 1.1];
  Toff = ((1-D)./P.fsw).*[0.9 1 1.1];
  P.fsw = fliplr(P.vout./(Ton.*P.vin));

  Icin = fliplr(P.iout.*sqrt(D.*(1-D)+(1/12).*((P.vout./(PS.Lo.Lk.*P.fsw.*P.iout)).^2).*((1-D).^2).*D));

  K = ((P.vin-P.vout).*D)./(PS.Lo.Lk.*P.iout.*P.fsw);
  ir = Ton.*(P.vin-P.vout)./fliplr(PS.Lo.Lk);

  R = PS.Co.esr;
  C = PS.Co.C;

  Cout_min = (PS.Lo.Lk.*(P.iout+0.5.*K.*P.iout).^2)./(P.vpeak.^2-P.vout.^2);

  Vout_min = -(ir.*R.^2.*C./Ton)-(ir./(2.*C.*Ton)).*((Ton./2).^2-(R.*C).^2);
  Vout_max = ir.*R.^2.*C./Toff + (ir./(2.*C.*Toff)).*((Toff./2).^2 - (R.*C).^2);

  Cin_min = fliplr(P.iout.*(D.*(1-D))./((P.vout(3)-P.vout(1)).*P.fsw));

  if (Ton./2)-R.*C>0 && (Toff./2)-R.*C>0
      Vout = Vout_max - Vout_min;
  end

  if R.*C > Ton./2 && R.*C > Toff./2
      Vout = ir.*R;
  end

  if R.*C > Ton./2 && R.*C < Toff./2
      Vout = Vout_max + ir.*R./2;
  end

  if R.*C < Ton./2 && R.*C > Toff./2
      Vout = -Vout_min + ir.*R./2;
  end

  fsw   = P.fsw./1e3;

  P.Tss = [1.8e-3, 3e-3, 4.2e-3];

  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Ton", Ton(1)*1e9, Ton(2)*1e9, Ton(3)*1e9, "ns");

  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Toff", Toff(1)*1e9, Toff(2)*1e9, Toff(3)*1e9, "ns");

  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	" Vout (DC)", P.vout(1), P.vout(2), P.vout(3), "V");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	" Recommended Cout for 1us load step", Cout_min(1)*1e6, Cout_min(2)*1e6, Cout_min(3)*1e6, "uF");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Recommended minimum Cin", Cin_min(1)*1e6, Cin_min(2)*1e6, Cin_min(3)*1e6, "uF");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Output ripple voltage", Vout(1)*1000, Vout(2)*1000, Vout(3)*1000, "mV");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Output ripple Current", ir(1), ir(2), ir(3), "A");

  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
    " Input capacitor Current", Icin(1), Icin(2), Icin(3), "A");


endfunction
