# Compute components from targets or compute
# achieved parameters with supplied components values
#
# P   : Target parameters
#       P.vin
#       P.vout
#       P.iout
#       P.fsw
#       P.k
# C   : Control components selection
#       C.R1
#       C.R2
#       C.R3
#       C.C1
#       C.Rton
# PS  : Power stage components selection
#       PS.Lo
#       PS.Co
#
# TODO: FB node ripple formula fix
#       Output voltage formula fix
function [P, C] = SC417(P, C, PS, name)
  # Chip parameters
  Prm.vref     = 0.5*[0.99, 1, 1.01];
  Prm.vrefldo  = [0.735, 0.75, 0.765];
  Prm.enlr     = [2.4, 2.6, 2.95];
  Prm.enlf     = [2.23, 2.4, 2.57];
  Prm.ilim_cs  = 10e-6*[0.999, 1, 1.001];  # Tolerances guessed
  Prm.ilim_ofs = [-10e-3, 0, 10e-3]*0;
  Prm.rdson    = [5.9, 7.4, 9.8]*1e-3; # Guest from spec: Rilim = 5.9k : Ilim = 6, 8, 10A
  Prm.Ton_min  = 80e-9;
  Prm.Toff_min = 250e-9;
  Prm.dV       = [10, 15, 20]*1e-3;    # Required ripple at FB pin, spec says min. 10mV


  Ctol = [0.95, 1, 1.05];
  Rtol = [0.99, 1, 1.01];

  ## 
  ## Components selection
  ## 
  if P.phase != 1
    error("Number of phase must be one")
  endif

  printf("=== SC417 component selection ===\n\n");

  printf("Selected components:\n")

  ## Output voltage
  if isfield(C, "R1") & !isfield(C, "R2")
    if isfield(C, "R3")
      R1eq = 1/(1/C.R1(2)+1/C.R3(2));
    else
      R1eq = C.R1(2);
    endif
    C.R2 = Prm.vref(2)*R1eq/(P.vout-Prm.vref(2))*Rtol;
  elseif !isfield(C, "R1") & isfield(C, "R2")
    R1eq = (P.vout - Prm.vref(2))*C.R2(2)./(Prm.vref(2));
    if isfield(C, "R3")
      C.R1 = R1eq*C.R3(2)/(C.R3(2)-R1eq)*Rtol;
    else
      C.R1 = R1eq*Rtol;
    endif
  elseif !isfield(C, "R1") & !isfield(C, "R2")
    error("R1 or R2 must be provided")
  endif
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (R1)", C.R1(2)/1e3, "kOhms");
  printf("%-45s : %6.2f %6s\n", " Voltage setting resistor (R2)", C.R2(2)/1e3, "kOhms");

  ## Switching frequency / Rton
  if !isfield(C, "Rton")
    Ton = P.vout/(P.vin(2)*P.fsw);
    C.Rton = (Ton-10e-9)*P.vin(2)/(25e-12*P.vout)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Ton setting resistor (Rton)", C.Rton(2)/1e3, "kOhms");
  if C.Rton > (P.vin(1)/15e-6)
    warning(" --> Rton too large")
  endif
  Ton   = (C.Rton*25e-12.*P.vout./fliplr(P.vin)+10e-9).*[0.9, 1, 1.1];
  P.fsw = P.vout./(fliplr(Ton).*P.vin);

  ## Minimum ripple at FB pin
  Zeq  = 1./sum(1./(PS.Co.esr + 1./(2*pi*P.fsw(2)*PS.Co.C*i)), 1); #effective at Fsw
  ESR = real(Zeq);
  Co  = -1./(2*pi*P.fsw.*imag(Zeq));
  dIL = fliplr(Ton).*(P.vin-P.vout)./fliplr(PS.Lo.Lk);
  dVo  = dIL.*abs(Zeq);
  if dVo.*(C.R2./(C.R1+C.R2)) < Prm.dV;
    if dVo >= Prm.dV
      if !isfield(C, "C1")
        printf(" Capacitive feed forward compensation needed:\n")
        C.C1 = \
            Ton(2)*((Prm.vref(2)+0.5*Prm.dV(2))*(C.R1(2)+C.R2(2))-P.vout*C.R2(2)) \
            / ((dVo(2)-Prm.dV(2))*C.R1(2)*C.R2(2))*Ctol;
      endif
    else
      if !isfield(C, "R3") || !isfield(C, "C1")
        if !isfield(P, "k")
          P.k = 3;
        endif
        printf(" Additionnal compensation ramp required:\n")
        Tau_off = P.k;            # 1 to 5 time constant voltage down slope during off time
        Tau_on  = Tau_off*(P.vout/(P.vin(2)-P.vout));
        k    = (1-exp(-Tau_on));
        C.R3 = (P.vin(2)./Prm.dV(2)*k-1)*C.R1(2)*C.R2(2)/(C.R1(2)+C.R2(2))*Rtol;
        C.C1 = Ton(2)/Tau_on*(1/C.R1(2)+1/C.R2(2)+1/C.R3(2))*Ctol;
      endif
    endif
  else
    if ESR < 3./(2*pi*Co*P.fsw(2))
      printf(" --> ESR loop instability!!!\n")
    endif
  endif
  if isfield(C, "C1")
    if isfield(C, "R3")
      printf("%-45s : %6.2f %6s\n", " Ramp compensation resistor (R3)", C.R3(2)/1e3, "kOhms");
      printf("%-45s : %6.0f %6s\n", " Ramp compensation capacitor (C1)", C.C1(2)*1e12, "pF");
    else
      printf("%-45s : %6.0f %6s\n", " Feed forward  compensation capacitor (C1)", C.C1(2)*1e12, "pF");
    endif
  endif

  ## Over current
  if !isfield(C, "Rilim")
    C.Rilim = ((P.ioc-dIL(1)/2)*Prm.rdson(3)+Prm.ilim_ofs(3))/Prm.ilim_cs(1)*Rtol;
  endif
  printf("%-45s : %6.2f %6s\n", " Over current limit resistor (Rilim)", C.Rilim(2)/1e3, "kOhms");
  printf("\n");

  ##
  ## Compute and printout achieved parameters
  ##
  printf("Achieved parameters:\n")
  printf("%35s   %5s, %5s, %5s %5s\n", " ", "min.", "typ.", "max.", " ");

  ## FB riple estimation
  if isfield(C, "C1")
    if isfield(C, "R3")
      ## Ramp compensation, neglecting Vout ripple in this case
      Tau = C.R1.*C.R2.*C.R3./(C.R1.*C.R2+C.R1.*C.R3+C.R2.*C.R3).*C.C1;
      K1 = C.R2.*C.R3./(C.R1.*C.R2+C.R1.*C.R3+C.R2.*C.R3);
      K2 = C.R1.*C.R2./(C.R1.*C.R2+C.R1.*C.R3+C.R2.*C.R3);

      for idx = [1,2,3]
        vin = P.vin(idx);
        ton = fliplr(Ton)(idx);
        Ts = ton*vin/P.vout;
        vref = Prm.vref(idx);
        for i = [1:10]
          ts = ton*(1-exp(Ts/Tau(idx)))*vin*K1(idx)/((exp(ton/Tau(idx))-1)*vin*K2(idx)+(1-exp(Ts/Tau(idx)))*vref);
          if abs((ts-Ts)/Ts) < 0.001
            break
          else
            Ts = ts;
          endif
        endfor
        if (ts-Ts)/Ts > 0.001
          error("No convergence")
        endif
        vout(idx) = vin*ton/Ts;
        P.fsw(idx) = 1/Ts;
        dVfb(idx) = exp(-ton./Tau(idx)).*((exp(2*ton/Tau(idx))-exp(ton/Tau(idx)))*vin*K2(idx)+
                                          (exp(ton/Tau(idx))-1)*exp(Ts/Tau(idx))*vout(idx)*K1(idx)+
                                          (1-exp(ton/Tau(idx)))*exp(Ts/Tau(idx))*vref);
      endfor
      #P.fsw = fliplr(P.fsw);
      dVfb = fliplr(dVfb);
    else
      ## Feed forward compensation, approxiamtion, could be replaced
      ## by exact formula
      dVfb = ((C.C1.*dVo.*C.R1+Ton*P.vout).*C.R2-Ton.*Prm.vref.*(C.R1+C.R2))./(C.C1.*C.R1.*C.R2+Ton/2.*(C.R1+C.R2));
    endif
  else
    ## ESR compensated
    dVfb = dVo.*C.R2./(C.R1+C.R2);
  endif
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " FB node ripple (pk-pk)", dVfb(1)*1e3, dVfb(2)*1e3, dVfb(3)*1e3, "mV");

  ## Switching frequency and ON time
  fsw   = P.fsw/1e3;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Ton", Ton(1)*1e9, Ton(2)*1e9, Ton(3)*1e9, "ns");
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
	 " Switching frequency", fsw(1), fsw(2), fsw(3), "kHz");

  ## output voltage
  if isfield(C, "R3")
    R1eq = 1./(1./C.R3 + 1./C.R1);
  else
    R1eq = C.R1;
  endif
  #P.vout = (Prm.vref+dVfb.*Ton.*P.fsw).*(R1eq./fliplr(C.R2)+1);
  P.vout = (Prm.vref+dVfb/2).*(R1eq./fliplr(C.R2)+1);
  #P.vout = (Prm.vref+dVo/2).*(C.R1./fliplr(C.R2)+1);
  #P.vout = (Prm.vref).*(R1eq./fliplr(C.R2)+1)+dVo/2;
  printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " Output voltage (average)", P.vout(1), P.vout(2), P.vout(3), "V");

  ## overcurrent
  P.ioc = (Prm.ilim_cs.*C.Rilim+Prm.ilim_ofs)./fliplr(Prm.rdson);
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (valley)", P.ioc(1), P.ioc(2), P.ioc(3), "A");
  P.ioc_avg = P.ioc + dIL/2;
  printf("%-35s : %5.1f, %5.1f, %5.1f %-5s\n",
  	 " Overcurrent (average)", P.ioc_avg(1), P.ioc_avg(2), P.ioc_avg(3), "A");

  ## Duty cycle limitation

  ## LDO output voltage
  if isfield(C, "Rf1") & isfield(C, "Rf2")
    vldo = Prm.vrefldo.*(C.Rf1./fliplr(C.Rf2)+1);
    printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " LDO output voltage", vldo(1), vldo(2), vldo(3), "V");
  endif

  ## UVLO
  if isfield(C, "Re1") & isfield(C, "Re2")
    uvlor = Prm.enlr.*(C.Re1./fliplr(C.Re2)+1);
    uvlof = Prm.enlf.*(C.Re1./fliplr(C.Re2)+1);
    printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " UVLO rising", uvlor(1), uvlor(2), uvlor(3), "V");
    printf("%-35s : %5.3f, %5.3f, %5.3f %-5s\n",
	 " UVLO falling", uvlof(1), uvlof(2), uvlof(3), "V");
  endif

  printf("\n");

endfunction
